package com.company;

import apr.prvadz.Matrix;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            zadatak2LU();
            zadatak2LUP();

            System.out.println("\n");

            try {
                zadatak3LU();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                zadatak3LUP();
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("\n");

            zadatak4LU();
            System.out.println("\n");

            zadatak4LUP();
            System.out.println("\n");

            zadatak5LUP();
            System.out.println("\n");

            zadatak6LUP();
            System.out.println("\n");

            try {
                zadatak7();
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("\n");

            zadatak8();
            System.out.println("\n");

            zadatak9();
            System.out.println("\n");

            zadatak10();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static void zadatak2LU() throws IOException {
        System.out.println("Zadatak 2(LU):");
        Matrix A = new Matrix("zad2A");
        Matrix b = new Matrix("zad2b");
        LU(A, b);
    }

    private static void zadatak2LUP() throws IOException {
        System.out.println("Zadatak 2(LUP):");
        Matrix A = new Matrix("zad2A");
        Matrix b = new Matrix("zad2b");
        LUP(A, b);
    }

    private static void zadatak3LU() throws IOException {
        System.out.println("Zadatak 3(LU):");
        Matrix A = new Matrix("zad3A");
        Matrix b = new Matrix("zad3b");
        LU(A, b);
    }

    private static void zadatak3LUP() throws IOException {
        System.out.println("Zadatak 3(LUP):");
        Matrix A = new Matrix("zad3A");
        Matrix b = new Matrix("zad3b");
        LUP(A, b);
    }

    private static void zadatak4LU() throws IOException {
        System.out.println("Zadatak 4(LU):");
        Matrix A = new Matrix("zad4A");
        Matrix b = new Matrix("zad4b");
        LU(A, b);
    }

    private static void zadatak4LUP() throws IOException {
        System.out.println("Zadatak 4(LUP):");
        Matrix A = new Matrix("zad4A");
        Matrix b = new Matrix("zad4b");
        LUP(A, b);
    }

    private static void zadatak5LUP() throws IOException {
        System.out.println("Zadatak 5(LUP):");
        Matrix A = new Matrix("zad5A");
        Matrix b = new Matrix("zad5b");
        LUP(A, b);
    }

    private static void zadatak6LUP() throws IOException {
        System.out.println("Zadatak 6(LUP):");
        Matrix A = new Matrix("zad6A");
        Matrix b = new Matrix("zad6b");

        A.multiplyRow(0, 1e-9);
        A.multiplyRow(2, 1e10);

        b.multiplyRow(0, 1e-9);
        b.multiplyRow(2, 1e10);

        LUP(A, b);
    }

    private static void zadatak7() throws IOException {
        System.out.println("Zadatak 7(inverz):");
        Matrix A = new Matrix("zad7A");
        inverse(A);
    }

    private static void zadatak8() throws IOException {
        System.out.println("Zadatak 8(inverz):");
        Matrix A = new Matrix("zad8A");
        inverse(A);
    }

    private static void zadatak9() throws IOException {
        System.out.println("Zadatak 9(det):");
        Matrix A = new Matrix("zad9A");
        determinant(A);
    }

    private static void zadatak10() throws IOException {
        System.out.println("Zadatak 10(det):");
        Matrix A = new Matrix("zad10A");
        determinant(A);
    }

    private static void LU(Matrix A, Matrix b) {
        A.decomposeLU();

        System.out.println("L:");
        A.printL();

        System.out.println("U:");
        A.printU();

        Matrix x = A.getX(b, null);

        System.out.println("x:");

        x.printToConsole();
    }

    private static void LUP(Matrix A, Matrix b) {
        Matrix P = A.decomposeLUP();

        System.out.println("L:");
        A.printL();

        System.out.println("U:");
        A.printU();

        Matrix x = A.getX(b, P);

        System.out.println("x:");

        x.printToConsole();
    }

    private static void inverse(Matrix A) {
        Matrix P = A.decomposeLUP();

        System.out.println("L:");
        A.printL();

        System.out.println("U:");
        A.printU();

        Matrix inv = A.inverse(P);

        System.out.println("inv:");

        inv.printToConsole();
    }

    private static void determinant(Matrix A) {
        Matrix P = A.decomposeLUP();

        Double det = A.determinant();
        System.out.println("det:\n" + det);
    }
}
