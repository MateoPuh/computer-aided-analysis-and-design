package apr.prvadz;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Matrix {

    private static double EPSILON = 1e-6;

    private int rows;
    private int columns;
    private boolean decomposed;
    private Double[][] content;
    private int numOfChanges;

    public Matrix(String filename) throws IOException, NumberFormatException {
        List<String> lines = Files.readAllLines(Paths.get(filename));
        rows = lines.size();

        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            String[] rowNums = line.trim().split("\\s");
            if (columns == 0) {
                columns = rowNums.length;
                content = new Double[rows][columns];
            }

            for (int j = 0; j < rowNums.length; j++) {
                content[i][j] = Double.parseDouble(rowNums[j]);
            }
        }
    }

    private Matrix(Double[][] content) {
        this.rows = content.length;
        this.columns = content[0].length;
        this.content = content;
    }

    public void addEquals(Matrix other) {
        if (rows != other.rows || columns != other.columns) {
            throw new RuntimeException("Matrices not compatible for addition.");
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                content[i][j] += other.content[i][j];
            }
        }
    }

    public Matrix add(Matrix other) {
        if (rows != other.rows || columns != other.columns) {
            throw new RuntimeException("Matrices not compatible for addition.");
        }

        Double[][] newContent = new Double[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                newContent[i][j] = content[i][j] + other.content[i][j];
            }
        }

        return new Matrix(newContent);
    }

    public void subEquals(Matrix other) {
        if (rows != other.rows || columns != other.columns) {
            throw new RuntimeException("Matrices not compatible for subtraction.");
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                content[i][j] -= other.content[i][j];
            }
        }
    }

    public Matrix sub(Matrix other) {
        if (rows != other.rows || columns != other.columns) {
            throw new RuntimeException("Matrices not compatible for subtraction.");
        }

        Double[][] newContent = new Double[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                newContent[i][j] = content[i][j] - other.content[i][j];
            }
        }

        return new Matrix(newContent);
    }

    public void decomposeLU() {
        if (!decomposed) {
            decomposed = true;
        }

        if (rows != columns) {
            throw new RuntimeException("Matrix cannot be LU decomposed.");
        }

        int n = rows;

        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (Math.abs(content[i][i] - 0.0) < EPSILON) {
                    throw new RuntimeException("Matrix error.");
                }

                content[j][i] /= content[i][i];

                for (int k = i + 1; k < n; k++) {
                    content[j][k] -= content[j][i] * content[i][k];
                }
            }
        }
    }

    public Matrix decomposeLUP() {
        if (!decomposed) {
            decomposed = true;
        }

        if (rows != columns) {
            throw new RuntimeException("Matrix cannot be LU decomposed.");
        }

        int n = rows;

        Matrix p = new Matrix(new Double[n][n]);
        initP(p);

        for (int i = 0; i < n - 1; i++) {
            double pivot = 0.0;
            int l = -1;

            for (int j = i; j < n; j++) {
                if (Math.abs(content[j][i]) > pivot) {
                    pivot = Math.abs(content[j][i]);
                    l = j;
                }
            }

            if (pivot == 0.0) {
                throw new RuntimeException("Matrix is singular.");
            }

            switchRows(content, n, i, l);
            switchRows(p.content, n, i, l);
            numOfChanges++;

            for (int j = i + 1; j < n; j++) {
                content[j][i] /= content[i][i];
                for (int k = i + 1; k < n; k++) {
                    content[j][k] -= content[j][i] * content[i][k];
                }
            }
        }

        return p;
    }

    public Matrix inverse(Matrix p) {
        if (rows != columns) {
            throw new RuntimeException("Matrix cannot be LU decomposed.");
        }

        int n = rows;

        if (p == null) {
            p = new Matrix(new Double[n][n]);
            initP(p);
        }

        Matrix result = new Matrix(new Double[n][n]);

        for (int i = 0; i < n; i++) {
            try {
                Matrix xi = getX(p.getColumn(i), null);

                for (int j = 0; j < n; j++) {
                    result.setElement(j, i, xi.getElement(j, 0));
                }
            } catch (Exception e) {
                throw new RuntimeException("Inverse doesn't exist for given matrix.");
            }
        }

        return result;
    }

    public Double determinant() {
        if (rows != columns) {
            throw new RuntimeException("Matrix cannot be LU decomposed.");
        }

        int multiplier = 1;
        int n = rows;

        if (numOfChanges % 2 != 0) {
            multiplier = -1;
        }

        double l = 1.0;
        double u = 1.0;

        for (int i = 0; i < n; i++) {
            l *= getLAt(i, i);
            u *= getUAt(i, i);
        }

        return multiplier * l * u;
    }

    private Matrix getRow(int row) {
        int n = rows;

        Matrix result = new Matrix(new Double[1][n]);

        for (int i = 0; i < n; i++) {
            result.setElement(row, i, getElement(row, i));
        }

        return result;
    }

    private Matrix getColumn(int column) {
        int n = rows;

        Matrix result = new Matrix(new Double[n][1]);

        for (int i = 0; i < n; i++) {
            result.setElement(i, 0, getElement(i, column));
        }

        return result;
    }

    private void switchRows(Double[][] content, int n, int row1, int row2) {
        for (int i = 0; i < n; i++) {
            double t = content[row1][i];
            content[row1][i] = content[row2][i];
            content[row2][i] = t;
        }
    }

    private Double getLAt(int row, int column) {
        if (row < 0 || row > rows || column < 0 || column > columns) {
            throw new IndexOutOfBoundsException();
        }

        if (column > row) {
            return 0.0;
        } else if (column == row) {
            return 1.0;
        } else {
            return getElement(row, column);
        }
    }

    private Double getUAt(int row, int column) {
        if (row < 0 || row > rows || column < 0 || column > columns) {
            throw new IndexOutOfBoundsException();
        }

        if (column < row) {
            return 0.0;
        } else {
            return getElement(row, column);
        }
    }

    private Matrix substituteForward(Matrix vector, Matrix p) {
        if (vector.columns != 1) {
            throw new RuntimeException();
        }

        if (p != null) {
            p.mul(vector);
            vector = p;
        }

        Double[][] newContent = new Double[vector.rows][vector.columns];

        for (int i = 0; i < vector.rows; i++) {
            Double sum = 0.0;

            for (int j = 0; j < i; j++) {
                sum += getLAt(i, j) * newContent[j][0];
            }

            newContent[i][0] = vector.content[i][0] - sum;
        }

        return new Matrix(newContent);
    }

    public Matrix getX(Matrix b, Matrix p) {
        Matrix y = substituteForward(b, p);

        return substituteBackward(y);
    }

    private Matrix substituteBackward(Matrix vector) {
        if (vector.columns != 1) {
            throw new RuntimeException();
        }

        Double[][] newContent = new Double[vector.rows][vector.columns];

        for (int i = vector.rows - 1; i >= 0; i--) {
            Double sum = 0.0;

            for (int j = i + 1; j < vector.rows; j++) {
                sum += getUAt(i, j) * newContent[j][0];
            }

            newContent[i][0] = vector.content[i][0] - sum;

            if (Math.abs(getUAt(i, i)) < EPSILON) {
                throw new RuntimeException("Division by 0");
            }

            newContent[i][0] /= getUAt(i, i);
        }

        return new Matrix(newContent);
    }

    private void mul(Matrix other) {
        if (columns != other.rows) {
            throw new RuntimeException();
        }

        Double[][] newContent = new Double[rows][other.columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < other.columns; j++) {
                newContent[i][j] = 0.0;
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < other.columns; j++) {
                for (int k = 0; k < columns; k++) {
                    newContent[i][j] += content[i][k] * other.content[k][j];
                }
            }
        }

        this.rows = newContent.length;
        this.columns = newContent[0].length;
        this.content = newContent;
    }

    public void transpose() {
        int newRows = columns;
        this.columns = rows;
        this.rows = newRows;

        Double[][] newContent = new Double[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                newContent[i][j] += content[j][i];
            }
        }

        this.content = newContent;
    }

    public void multiplyWithScalar(double scalar) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                content[i][j] *= scalar;
            }
        }
    }

    private Double getElement(int row, int column) {
        if (row < 0 || row > rows || column < 0 || column > columns) {
            throw new IndexOutOfBoundsException();
        }

        return content[row][column];
    }

    private void setElement(int row, int column, double value) {
        if (row < 0 || row > rows || column < 0 || column > columns) {
            throw new IndexOutOfBoundsException();
        }

        content[row][column] = value;
    }

    public void saveToFile(String filename) throws FileNotFoundException {
        PrintWriter out = new PrintWriter(filename);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (j > 0) {
                    out.print(" ");
                }
                out.print(content[i][j]);
            }
            out.print("\n");
        }

        out.close();
    }

    public void printToConsole() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (j > 0) {
                    System.out.print(" ");
                }
                System.out.print(String.format("%.2f",content[i][j]));
            }
            System.out.print("\n");
        }
    }

    public void printL() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (j > 0) {
                    System.out.print(" ");
                }
                System.out.print(String.format("%.2f",getLAt(i, j)));
            }
            System.out.print("\n");
        }
    }

    public void printU() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (j > 0) {
                    System.out.print(" ");
                }
                System.out.print(String.format("%.2f",getUAt(i, j)));
            }
            System.out.print("\n");
        }
    }

    public void multiplyRow(int row, double scalar) {
        for (int i = 0; i < columns; i++) {
            content[row][i] *= scalar;
        }
    }

    private void initP(Matrix p) {
        for (int i = 0; i < p.rows; i++) {
            for (int j = 0; j < p.rows; j++) {
                if (i == j) {
                    p.setElement(i, i, 1.0);
                } else {
                    p.setElement(i, j, 0.0);
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return rows == matrix.rows &&
                columns == matrix.columns &&
                Arrays.equals(content, matrix.content);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(rows, columns);
        result = 31 * result + Arrays.hashCode(content);
        return result;
    }
}
