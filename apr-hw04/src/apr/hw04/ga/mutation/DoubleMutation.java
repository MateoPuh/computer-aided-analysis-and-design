package apr.hw04.ga.mutation;

import apr.hw04.ga.chromosome.DoubleChromosome;

public interface DoubleMutation {

    void mutate(DoubleChromosome c, double pm);
}
