package apr.hw04.ga.mutation;

import apr.hw04.ga.chromosome.DoubleChromosome;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DoubleGaussianMutation implements DoubleMutation {

    private static Random random = new Random();

    private double dg;
    private double gg;

    public DoubleGaussianMutation(double dg, double gg) {
        this.dg = dg;
        this.gg = gg;
    }

    @Override
    public void mutate(DoubleChromosome c, double pm) {
        List<Double> newX = new ArrayList<>();

        for (Double x : c.getX()) {
            double r = random.nextGaussian() * ((gg - dg) / 10) + x;
            if (r > gg) r = gg;
            if (r < dg) r = dg;
            newX.add(r);
        }

        c.setX(newX);
    }
}
