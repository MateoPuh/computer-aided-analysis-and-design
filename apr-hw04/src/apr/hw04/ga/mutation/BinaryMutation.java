package apr.hw04.ga.mutation;

import apr.hw04.ga.Utils;
import apr.hw04.ga.chromosome.BinaryChromosome;
import apr.hw04.ga.chromosome.ChromosomeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public interface BinaryMutation {

    BinaryMutation simpleMutation = new BinaryMutation() {
        Random rand = new Random();

        @Override
        public void mutate(BinaryChromosome c, double pm) {
            List<String> mutated = new ArrayList<>();

            for (String bs : c.getB()) {
                Boolean[] b = ChromosomeUtils.fromString(bs);

                for (int i = 0; i < b.length; i++) {
                    if (rand.nextDouble() < pm) {
                        b[i] = !b[i];
                    }
                }

                mutated.add(Utils.binaryString(ChromosomeUtils.fromBinary(b, c.getN()), c.getN()));
            }

            c.setB(mutated);
        }
    };

    void mutate(BinaryChromosome c, double pm);
}
