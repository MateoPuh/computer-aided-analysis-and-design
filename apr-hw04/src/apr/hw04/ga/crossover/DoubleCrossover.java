package apr.hw04.ga.crossover;

import apr.hw04.ga.chromosome.DoubleChromosome;

public interface DoubleCrossover {

    DoubleChromosome crossover(DoubleChromosome c1, DoubleChromosome c2);
}
