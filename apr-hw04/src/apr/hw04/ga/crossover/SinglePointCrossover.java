package apr.hw04.ga.crossover;

import apr.hw04.ga.chromosome.BinaryChromosome;
import apr.hw04.ga.chromosome.ChromosomeUtils;

import java.util.ArrayList;
import java.util.List;

public class SinglePointCrossover implements BinaryCrossover {

    private int n;

    public SinglePointCrossover(int n) {
        this.n = n;
    }

    @Override
    public BinaryChromosome crossover(BinaryChromosome c1, BinaryChromosome c2) {
        List<Integer> mutated = new ArrayList<>();

        for (int i = 0; i < c1.getB().size(); i++) {
            String firstPart = c1.getB().get(i).substring(0, n);
            String secondPart = c2.getB().get(i).substring(n);
            Boolean[] b = ChromosomeUtils.fromString(firstPart + secondPart);
            mutated.add(ChromosomeUtils.fromBinary(b, c1.getN()));
        }

        return new BinaryChromosome(mutated, c1.getDg(), c1.getGg(), c1.getN());
    }
}
