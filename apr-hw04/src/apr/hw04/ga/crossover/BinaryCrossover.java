package apr.hw04.ga.crossover;

import apr.hw04.ga.chromosome.BinaryChromosome;
import apr.hw04.ga.chromosome.ChromosomeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public interface BinaryCrossover {

    BinaryCrossover uniform = new BinaryCrossover() {
        Random rand = new Random();

        @Override
        public BinaryChromosome crossover(BinaryChromosome c1, BinaryChromosome c2) {
            StringBuilder sb = new StringBuilder();

            List<Integer> mutated = new ArrayList<>();

            for (int j = 0; j < c1.getB().size(); j++) {
                sb = new StringBuilder();
                for (int i = 0; i < c1.getB().get(j).length(); i++) {
                    if (rand.nextDouble() < 0.5) {
                        sb.append(c1.getB().get(j).charAt(i));
                    } else {
                        sb.append(c2.getB().get(j).charAt(i));
                    }
                }

                Boolean[] b = ChromosomeUtils.fromString(sb.toString());

                mutated.add(ChromosomeUtils.fromBinary(b, c1.getN()));
            }

            return new BinaryChromosome(mutated, c1.getDg(), c1.getGg(), c1.getN());
        }
    };

    BinaryChromosome crossover(BinaryChromosome c1, BinaryChromosome c2);
}
