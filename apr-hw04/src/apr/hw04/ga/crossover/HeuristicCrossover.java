package apr.hw04.ga.crossover;

import apr.hw04.ga.chromosome.DoubleChromosome;

import java.util.ArrayList;
import java.util.List;

public class HeuristicCrossover implements DoubleCrossover {

    private double a;

    public HeuristicCrossover(double a) {
        this.a = a;
    }

    @Override
    public DoubleChromosome crossover(DoubleChromosome c1, DoubleChromosome c2) {
        List<Double> mutated = new ArrayList<>();

        for (int j = 0; j < c1.getX().size(); j++) {
            double x2 = (c1.getFitness() > c2.getFitness()) ? c1.getX().get(j) : c2.getX().get(j);
            double x1 = (c1.getFitness() < c2.getFitness()) ? c1.getX().get(j) : c2.getX().get(j);

            mutated.add(a * (x2 - x1) + x2);
        }

        return new DoubleChromosome(mutated, c1.getDg(), c2.getGg());
    }
}
