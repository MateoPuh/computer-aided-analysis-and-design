package apr.hw04.ga.crossover;

import apr.hw04.ga.chromosome.DoubleChromosome;

import javax.print.attribute.standard.PrinterMakeAndModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ArithmeticCrossover implements DoubleCrossover {

    private static Random random = new Random();

    public ArithmeticCrossover() { }

    @Override
    public DoubleChromosome crossover(DoubleChromosome c1, DoubleChromosome c2) {
        List<Double> mutated = new ArrayList<>();

        for (int j = 0; j < c1.getX().size(); j++) {
            double a = random.nextDouble();
            mutated.add(c1.getX().get(j) * a + (1 - a) * c2.getX().get(j));
        }

        return new DoubleChromosome(mutated, c1.getDg(), c2.getGg());
    }
}
