package apr.hw04.ga.crossover;

import apr.hw04.ga.chromosome.BinaryChromosome;
import apr.hw04.ga.chromosome.ChromosomeUtils;

import java.util.ArrayList;
import java.util.List;

public class MultiPointCrossover implements BinaryCrossover {

    private Integer[] n;

    public MultiPointCrossover(Integer[] n) {
        this.n = n;
    }

    @Override
    public BinaryChromosome crossover(BinaryChromosome c1, BinaryChromosome c2) {
        List<Integer> mutated = new ArrayList<>();

        for (int i = 0; i < c1.getB().size(); i++) {
            StringBuilder bString = new StringBuilder(c1.getB().get(i).substring(0, n[0]));
            boolean isC1 = false;

            for (int j = 0; j < n.length - 2; j++) {
                bString.append(isC1 ? c1.getB().get(i).substring(n[j], n[j + 1]) : c2.getB().get(i).substring(n[j], n[j + 1]));
                isC1 = !isC1;
            }

            bString.append(isC1 ? c1.getB().get(i).substring(n[n.length - 1]) : c2.getB().get(i).substring(n[n.length - 1]));

            Boolean[] b = ChromosomeUtils.fromString(bString.toString());

            mutated.add(ChromosomeUtils.fromBinary(b, c1.getN()));
        }

        return new BinaryChromosome(mutated, c1.getDg(), c1.getGg(), c1.getN());
    }
}
