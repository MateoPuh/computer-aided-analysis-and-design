package apr.hw04.ga.ga;

import apr.hw04.ga.Utils;
import apr.hw04.ga.chromosome.BinaryChromosome;
import apr.hw04.ga.chromosome.Chromosome;
import apr.hw04.ga.chromosome.ChromosomeUtils;
import apr.hw04.ga.crossover.BinaryCrossover;
import apr.hw04.ga.function.Function;
import apr.hw04.ga.mutation.BinaryMutation;
import apr.hw04.ga.selection.EliminationalTournament;

import java.util.List;
import java.util.stream.Collectors;

public class BinaryEliminationalGA extends GeneticAlgorithm<BinaryChromosome> {

    public BinaryEliminationalGA(Function f, int k, double dg, double gg, int size, int chromosomeSize, int p, double pm, int iterLimit, boolean print) {
        super(f, new EliminationalTournament(k), true, dg, gg, size, chromosomeSize, p, pm, iterLimit, print);
    }

    @Override
    public void printPopulation() {
        for (BinaryChromosome c : population) {
            System.out.print(ChromosomeUtils.getX(c.getbInt().get(0), dg, gg, c.getN()) + " ");
            System.out.println(ChromosomeUtils.getX(c.getbInt().get(1), dg, gg, c.getN()));

//            System.out.println("chromosome: " + c.toString() + ", value: " + c.getValue(f) + ", fitness: " + c.getFitness());
        }
    }

    @Override
    public Chromosome evolve(int everyN, double min, double epsilon) {
        int iter = 0;
        Chromosome best;

        while(true) {
            oneStep();

            best = Utils.getBest(population.stream().map(it -> (Chromosome)it).collect(Collectors.toList()));

            if (iter % everyN == 0 && print) {
                System.out.println("Population on " + iter + "th iteration:");
                printPopulation();
                System.out.println("Best chromosome: " + best + "Best value: " + best.getValue(f));
                System.out.println();
            }

            if (Math.abs(best.getValue(f) - min) < epsilon) {
                break;
            }

            iter++;
            if (iter > iterLimit) {
                break;
            }
        }

        return best;
    }

    private void oneStep() {
        List<BinaryChromosome> parents = selection.select(population, 1);
        BinaryChromosome newChromosome = BinaryCrossover.uniform.crossover(parents.get(0), parents.get(1));
        BinaryMutation.simpleMutation.mutate(newChromosome, pm);
        population.add(newChromosome);
        setFitness();
    }
}
