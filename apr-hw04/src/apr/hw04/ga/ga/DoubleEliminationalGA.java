package apr.hw04.ga.ga;

import apr.hw04.ga.Utils;
import apr.hw04.ga.chromosome.Chromosome;
import apr.hw04.ga.chromosome.DoubleChromosome;
import apr.hw04.ga.crossover.ArithmeticCrossover;
import apr.hw04.ga.function.Function;
import apr.hw04.ga.mutation.DoubleGaussianMutation;
import apr.hw04.ga.selection.DoubleEliminationalTournament;

import java.util.List;
import java.util.stream.Collectors;

public class DoubleEliminationalGA extends GeneticAlgorithm<DoubleChromosome> {

    public DoubleEliminationalGA(Function f, int k, double dg, double gg, int size, int chromosomeSize, int p, double pm, int iterLimit, boolean print) {
        super(f, new DoubleEliminationalTournament(k), false, dg, gg, size, chromosomeSize, p, pm, iterLimit, print);
    }

    @Override
    public void printPopulation() {
        for (DoubleChromosome c : population) {
            System.out.println("chromosome: " + c.toString() + ", value: " + c.getValue(f) + ", fitness: " + c.getFitness());
        }
    }

    @Override
    public Chromosome evolve(int everyN, double min, double epsilon) {
        int iter = 0;
        Chromosome best;

        while(true) {
            oneStep();

            best = Utils.getBest(population.stream().map(it -> (Chromosome)it).collect(Collectors.toList()));

            if (iter % everyN == 0 && print) {
                System.out.println("Population on " + iter + "th iteration:");
                printPopulation();
                System.out.println("Best chromosome: " + best + "Best value: " + best.getValue(f));
                System.out.println();
            }

            if (Math.abs(best.getValue(f) - min) < epsilon) {
                break;
            }

            iter++;
            if (iter > iterLimit) {
                break;
            }
        }

        return best;
    }

    private void oneStep() {
        List<DoubleChromosome> parents = selection.select(population, 1);
        DoubleChromosome newChromosome = new ArithmeticCrossover().crossover(parents.get(0), parents.get(1));
        new DoubleGaussianMutation(dg, gg).mutate(newChromosome, pm);
        population.add(newChromosome);
        setFitness();
    }
}
