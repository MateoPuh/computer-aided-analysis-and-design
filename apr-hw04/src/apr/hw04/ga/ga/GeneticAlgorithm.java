package apr.hw04.ga.ga;

import apr.hw04.ga.chromosome.BinaryChromosome;
import apr.hw04.ga.chromosome.Chromosome;
import apr.hw04.ga.chromosome.DoubleChromosome;
import apr.hw04.ga.function.Function;
import apr.hw04.ga.selection.Selection;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class GeneticAlgorithm<T extends Chromosome> {

    protected List<T> population;
    protected Function f;
    protected Selection selection;
    protected boolean binary;
    protected double dg;
    protected double gg;
    protected int size;
    protected int chromosomeSize;
    protected int p;
    protected double pm;
    protected int iterLimit;
    protected boolean print;

    protected GeneticAlgorithm(Function f, Selection selection, boolean binary, double dg, double gg, int size, int chromosomeSize, int p, double pm, int iterlimit, boolean print) {
        this.f = f;
        this.selection = selection;
        this.binary = binary;
        this.dg = dg;
        this.gg = gg;
        this.size = size;
        this.chromosomeSize = chromosomeSize;
        this.p = p;
        this.pm = pm;
        this.iterLimit = iterlimit;
        this.print = print;
    }

     public List<T> generateInitialPopulation() {
        Random rand = new Random();
        List<T> res = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            List<Double> x = new ArrayList<>();

            for (int j = 0; j < chromosomeSize; j++) {
                x.add(dg + rand.nextDouble() * (gg - dg));
            }

            if (binary) {
                res.add((T)new BinaryChromosome(x, p, dg, gg));
            } else {
                res.add((T)new DoubleChromosome(x, dg, gg));
            }
        }
        population = res;

        setFitness();

        return res;
    }

    public void setFitness() {
        Double worst = null;

        for (Chromosome c : population) {
            double v = c.getValue(f);

            if (worst == null) {
                worst = v;
            }

            if (v > worst) {
                worst = v;
            }
        }

        for (Chromosome c : population) {
            double v = c.getValue(f);

            c.setFitness(worst - v);
        }
    }

    public abstract void printPopulation();

    public abstract Chromosome evolve(int everyN, double min, double epsilon);
}
