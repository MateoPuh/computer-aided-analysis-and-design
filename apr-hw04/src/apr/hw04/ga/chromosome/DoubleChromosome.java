package apr.hw04.ga.chromosome;

import apr.hw04.ga.function.Function;

import java.util.List;

public class DoubleChromosome extends Chromosome {

    private List<Double> x;

    public DoubleChromosome(List<Double> x, double dg, double gg) {
        this.x = x;
        this.dg = dg;
        this.gg = gg;
    }

    @Override
    public void print() {
        System.out.println(x);
    }

    @Override
    public double getValue(Function f) {
        return f.doFunction(x);
    }

    public List<Double> getX() {
        return x;
    }

    public void setX(List<Double> x) {
        this.x = x;
    }

    @Override
    public String toString() {
        return String.valueOf(x);
    }
}
