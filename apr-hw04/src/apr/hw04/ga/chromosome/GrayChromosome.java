package apr.hw04.ga.chromosome;

import apr.hw04.ga.Utils;
import apr.hw04.ga.function.Function;

import java.util.List;
import java.util.stream.Collectors;

public class GrayChromosome extends Chromosome {

    private List<String> g;
    private List<Integer> gInt;
    private int n;

    public GrayChromosome(List<Integer> gIng, double dg, double gg, int n) {
        this.gInt = gInt;
        this.g = gInt.stream().map(it -> Utils.binaryString(it, n)).collect(Collectors.toList());
        this.n = n;
        this.dg = dg;
        this.gg = gg;
    }

    @Override
    public void print() {
        System.out.println(g);
    }

    @Override
    public double getValue(Function f) {
        return f.doFunction(gInt.stream().map(it -> (double) it).collect(Collectors.toList()));
    }

    public List<String> getG() {
        return g;
    }

    public void setG(List<String> g) {
        this.g = g;
    }

    public List<Integer> getgInt() {
        return gInt;
    }

    public void setgInt(List<Integer> gInt) {
        this.gInt = gInt;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    @Override
    public String toString() {
        return String.valueOf(g);
    }
}
