package apr.hw04.ga.chromosome;

import apr.hw04.ga.function.Function;

public abstract class Chromosome {

    protected double dg;
    protected double gg;
    protected double fitness;

    public abstract void print();

    public abstract double getValue(Function f);

    public double getDg() {
        return dg;
    }

    public void setDg(double dg) {
        this.dg = dg;
    }

    public double getGg() {
        return gg;
    }

    public void setGg(double gg) {
        this.gg = gg;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }
}
