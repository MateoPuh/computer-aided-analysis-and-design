package apr.hw04.ga.chromosome;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ChromosomeUtils {

    public static int getN(int p, double dg, double gg) {
        return (int)Math.ceil(Math.log(Math.floor(1 + (gg - dg) * Math.pow(10, p))) / Math.log(2));
    }

    public static int fromBinary(Boolean[] b, int n) {
        int res = 0;
        int mul = 1;

        for (int i = n-1; i >= 0; i--) {
            if (b[i]) {
                res += mul;
            }
            mul *= 2;
        }

        return res;
    }

    public static GrayChromosome binaryToGray(BinaryChromosome bc) {
        List<Integer> grayRes = new ArrayList<>();

        for (String bs : bc.getB()) {
            Boolean[] res = new Boolean[bc.getN()];

            Boolean[] b = fromString(bs);

            res[0] = b[0];

            for (int i = 2; i < bc.getN(); i++) {
                res[i] = b[i-1] != b[i];
            }

            grayRes.add(fromBinary(res, bc.getN()));
        }

        return new GrayChromosome(grayRes, bc.getDg(), bc.getGg(), bc.getN());
    }

    public BinaryChromosome grayToBinary(GrayChromosome gc) {
        List<Integer> bRes = new ArrayList<>();

        for (String gs : gc.getG()) {
            Boolean[] res = new Boolean[gc.getN()];

            Boolean[] g = fromString(gs);

            boolean v = res[0] = g[0];

            for (int i = 2; i < gc.getN(); i++) {
                if (g[i]) {
                    v = !v;
                }

                res[i] = v;
            }

            bRes.add(fromBinary(res, gc.getN()));
        }

        return new BinaryChromosome(bRes, gc.getDg(), gc.getGg(), gc.getN());
    }

    public static int getB(double x, double dg, double gg, int n) {
        return (int)(((x - dg) / (gg - dg)) * (Math.pow(2, n) - 1));
    }

    public DoubleChromosome binaryToDouble(BinaryChromosome bc) {
        return new DoubleChromosome(
                bc.getbInt().stream().map(it -> bc.getDg() + (it / (Math.pow(2, bc.getN()) - 1)) * (bc.getGg() - bc.getDg())).collect(Collectors.toList()),
                bc.getDg(),
                bc.getGg());
    }

    public static Boolean[] fromString(String x) {
        Boolean[] res = new Boolean[x.length()];

        for (int i = 0; i < x.length(); i++) {
            res[i] = x.charAt(i) == '1';
        }

        return res;
    }

    public static double getX(int b, double dg, double gg, int n) {
        return dg + (b * (gg - dg)) / (Math.pow(2, n) - 1);
    }
}
