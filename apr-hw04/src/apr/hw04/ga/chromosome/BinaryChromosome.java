package apr.hw04.ga.chromosome;

import apr.hw04.ga.Utils;
import apr.hw04.ga.function.Function;

import java.util.List;
import java.util.stream.Collectors;

public class BinaryChromosome extends Chromosome {

    private List<String> b;
    private List<Integer> bInt;
    private int n;

    public BinaryChromosome(List<Integer> bInt, double dg, double gg, int n) {
        this.n = n;
        this.bInt = bInt;
        this.b = bInt.stream().map(it -> Utils.binaryString(it, n)).collect(Collectors.toList());
        this.dg = dg;
        this.gg = gg;
    }

    public BinaryChromosome(List<Double> x, int p, double dg, double gg) {
        this.n = ChromosomeUtils.getN(p, dg, gg);
        this.bInt = x.stream().map(it -> ChromosomeUtils.getB(it, dg, gg, n)).collect(Collectors.toList());
        this.b = bInt.stream().map(it -> Utils.binaryString(it, n)).collect(Collectors.toList());
        this.dg = dg;
        this.gg = gg;
    }

    @Override
    public void print() {
        System.out.println(b);
    }

    @Override
    public double getValue(Function f) {
        return f.doFunction(bInt.stream().map(it -> ChromosomeUtils.getX(it, dg, gg, n)).collect(Collectors.toList()));
    }

    public List<String> getB() {
        return b;
    }

    public void setB(List<String> b) {
        this.b = b;
    }

    public List<Integer> getbInt() {
        return bInt;
    }

    public void setbInt(List<Integer> bInt) {
        this.bInt = bInt;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (String s : b) {
            sb.append(s).append(" ");
        }

        return sb.toString();
    }
}
