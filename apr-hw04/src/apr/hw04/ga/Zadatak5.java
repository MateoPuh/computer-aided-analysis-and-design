package apr.hw04.ga;

import apr.hw04.ga.function.Function;

public class Zadatak5 {

    private static double dg = -50;
    private static double gg = 150;
    private static int size = 100;
    private static int p = 3;
    private static int[] ks = { 3, 6, 9, 12};
    private static double pm = 0.2;
    private static int iterLimit = 500000;
    private static boolean print = false;
    private static int dim = 2;

    public static void main(String[] args) {
        f6();
        f7();
    }

    private static void fbin(Function f, int k) {
        Utils.printBinRes(Utils.fbin(f, dim, k, dg, gg, size, dim, p, pm, iterLimit, print), dim, f, dg, gg);
    }

    private static void f(Function f, int k) {
        Utils.printDoubleRes(Utils.fdouble(f, dim, k, dg, gg, size, dim, p, pm, iterLimit, print), f);
    }


    private static void f6() {
        Function f6 = Function.function6();

        for (Integer k : ks) {
            System.out.println("FUNKCIJA 6, K " + k);
            f(f6, k);
            System.out.println();
        }
    }

    private static void f7() {
        Function f7 = Function.function7();

        for (Integer k : ks) {
            System.out.println("FUNKCIJA 7, K " + k);
            f(f7, k);
            System.out.println();
        }
    }
}
