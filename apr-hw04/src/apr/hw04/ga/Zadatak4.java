package apr.hw04.ga;

import apr.hw04.ga.chromosome.BinaryChromosome;
import apr.hw04.ga.chromosome.Chromosome;
import apr.hw04.ga.chromosome.DoubleChromosome;
import apr.hw04.ga.function.Function;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class Zadatak4 {

    private static double dg = -50;
    private static double gg = 150;
    private static int[] sizes = { 30, 50, 100, 200 };
    private static int p = 3;
    private static int k = 3;
    private static double[] pms = { 0.1, 0.3, 0.6, 0.9 };
    private static double pm = 0.21;
    private static int iterLimit = 200000;
    private static boolean print = false;
    private static int dim = 2;
    private static double EPSILON = 1e-4;

    public static void main(String[] args) {
        f6();
    }

    private static Chromosome fdouble(Function f, int size) {
        BinaryChromosome r = Utils.fbin(f, dim, k, dg, gg, size, dim, p, pm, iterLimit, print);
        Utils.printBinRes(r, dim, f, dg, gg);
        return r;
    }

    private static DoubleChromosome fdouble(Function f, int size, double pm) {
        DoubleChromosome r = Utils.fdouble(f, dim, k, dg, gg, size, dim, p, pm, iterLimit, print);
        Utils.printDoubleRes(r, f);
        return r;
    }

    private static void f6() {
        Function f6 = Function.function6();

        Pair<Integer, Double> res = null;

        List<Chromosome> res1list = new ArrayList<>();

        for (Integer s : sizes) {
            System.out.println("FUNKCIJA 6, SIZE " + s);
            Chromosome d = fdouble(f6, s);

            if (res == null) {
                res = new Pair<>(s, d.getValue(f6));
            }

            if (Math.abs(d.getValue(f6)) < res.getValue()) {
                res = new Pair<>(s, d.getValue(f6));
            }

            res1list.add(d);

            System.out.println();
        }

        Pair<Double, Double> res2 = null;

        List<DoubleChromosome> res2list = new ArrayList<>();

        for (Double p : pms) {
            System.out.println("FUNKCIJA 6, PM " + p);
            DoubleChromosome d = fdouble(f6, res.getKey(), p);

            if (res2 == null) {
                res2 = new Pair<>(p, d.getValue(f6));
            }

            if (Math.abs(d.getValue(f6)) < res2.getValue()) {
                res2 = new Pair<>(p, d.getValue(f6));
            }

            res2list.add(d);

            System.out.println();
        }
    }
}
