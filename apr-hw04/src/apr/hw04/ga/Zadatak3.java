package apr.hw04.ga;

import apr.hw04.ga.chromosome.BinaryChromosome;
import apr.hw04.ga.chromosome.ChromosomeUtils;
import apr.hw04.ga.chromosome.DoubleChromosome;
import apr.hw04.ga.function.Function;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Zadatak3 {

    private static double dg = -50;
    private static double gg = 150;
    private static int size = 50;
    private static int p = 3;
    private static int k = 3;
    private static double pm = 0.21;
    private static int iterLimit = 100000;
    private static boolean print = false;
    private static int[] dims = {3, 6};
    private static int numOfIter = 11;
    private static double EPSILON = 1e-2;

    public static void main(String[] args) {
        f6();
        f7();
    }

    private static void fbin(Function f, int dim, double min) {
        List<BinaryChromosome> res = new ArrayList<>();

        for (int i = 0; i < numOfIter; i++) {
            BinaryChromosome b = Utils.fbin(f, dim, k, dg, gg, size, dim, p, pm, iterLimit, print);
            res.add(b);
        }

        res.sort(new Comparator<BinaryChromosome>() {
            @Override
            public int compare(BinaryChromosome bin1, BinaryChromosome bin2) {
                double res = Math.abs(bin1.getValue(f) - min) - Math.abs(bin2.getValue(f) - min);
                if (Math.abs(res) < EPSILON) return 0;
                return res > 0 ? 1 : -1;
            }
        });

        int pogodak = 0;
        for (int i = 0; i < numOfIter; i++) {
            if (Math.abs(res.get(i).getValue(f) - min) < EPSILON) pogodak ++;
        }
        System.out.println("Results: " + res);
        for (int i = 0; i < numOfIter; i++) {
            for (int j = 0; j < dim; j++) {
                System.out.print(ChromosomeUtils.getX(res.get(i).getbInt().get(j), dg, gg, res.get(i).getN()) + " ");
            }
            System.out.print(", ");
        }
        System.out.println();
        System.out.println("Broj pogodaka: " + pogodak);
        System.out.println("Median: " +  res.get(5));
        for (int i = 0; i < dim; i++) {
            System.out.print(ChromosomeUtils.getX(res.get(5).getbInt().get(i), dg, gg, res.get(5).getN()) + " ");
        }
    }

    private static void fdouble(Function f, int dim) {
        List<DoubleChromosome> res = new ArrayList<>();

        for (int i = 0; i < numOfIter; i++) {
            res.add(Utils.fdouble(f, dim, k, dg, gg, size, dim, p, pm, iterLimit, print));
        }
    }

    private static void f6() {
        Function f6 = Function.function6();

        for (Integer d : dims) {
            System.out.println("FUNKCIJA 6, DIM " + d);
            fbin(f6, d, 0);
            System.out.println();
        }
    }

    private static void f7() {
        Function f7 = Function.function7();

        for (Integer d : dims) {
            System.out.println("FUNKCIJA 7, DIM " + d);
            fbin(f7, d, 0);
            System.out.println();
        }
    }
}
