package apr.hw04.ga.selection;

import apr.hw04.ga.Utils;
import apr.hw04.ga.chromosome.BinaryChromosome;
import apr.hw04.ga.chromosome.Chromosome;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public interface Selection<T extends Chromosome> {

    Selection rouletteWheel = new Selection<BinaryChromosome>() {
        Random rand = new Random();

        @Override
        public List<BinaryChromosome> select(List<BinaryChromosome> population, int n) {
            double total = Utils.totalFitness(population.stream().map(it -> (Chromosome)it).collect(Collectors.toList()));

            List<BinaryChromosome> res = new ArrayList<>();

            for (int i = 0; i < n; i++) {
                double r = rand.nextDouble() * total;

                double curr = 0;
                for (BinaryChromosome c : population) {
                    curr += c.getFitness();

                    if (r < curr) {
                        res.add(c);
                        break;
                    }
                }
            }

            return res;
        }
    };

    Selection elemination = new Selection<BinaryChromosome>() {
        Random rand = new Random();

        @Override
        public List<BinaryChromosome> select(List<BinaryChromosome> population, int n) {
            Chromosome best = Utils.getBest(population.stream().map(it -> (Chromosome)it).collect(Collectors.toList()));

            for (int i = 0; i < n; i++) {
                double total = Utils.totalPunishment(population.stream().map(it -> (Chromosome)it).collect(Collectors.toList()), best.getFitness());
                double r = rand.nextDouble() * total;

                double curr = 0;
                for (Chromosome c : population) {
                    curr += best.getFitness() - c.getFitness();

                    if (r < curr) {
                        population.remove(c);
                        break;
                    }
                }
            }

            return population;
        }
    };

    Selection tournament = new Selection<BinaryChromosome>() {
        Random rand = new Random();

        @Override
        public List<BinaryChromosome> select(List<BinaryChromosome> population, int n) {
            Chromosome best = Utils.getBest(population.stream().map(it -> (Chromosome)it).collect(Collectors.toList()));

            for (int i = 0; i < n; i++) {
                double total = Utils.totalPunishment(population.stream().map(it -> (Chromosome)it).collect(Collectors.toList()), best.getFitness());
                double r = rand.nextDouble() * total;

                double curr = 0;
                for (Chromosome c : population) {
                    curr += best.getFitness() - c.getFitness();

                    if (r < curr) {
                        population.remove(c);
                        break;
                    }
                }
            }

            return population;
        }
    };

    List<T> select(List<T> population, int n);
}
