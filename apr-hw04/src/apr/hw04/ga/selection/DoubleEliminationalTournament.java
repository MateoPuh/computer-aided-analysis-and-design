package apr.hw04.ga.selection;

import apr.hw04.ga.chromosome.DoubleChromosome;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DoubleEliminationalTournament implements Selection<DoubleChromosome> {

    private int k;
    private Random rand = new Random();

    public DoubleEliminationalTournament(int k) {
        this.k = k;
    }

    @Override
    public List<DoubleChromosome> select(List<DoubleChromosome> population, int n) {
        int i = rand.nextInt(population.size());
        int j = rand.nextInt(population.size());
        while (j == i) {
            j = rand.nextInt(population.size());
        }

        int k = rand.nextInt(population.size());
        while (k == j || k == i) {
            k = rand.nextInt(population.size());
        }

        List<DoubleChromosome> res = new ArrayList<>();

        double fi = population.get(i).getFitness();
        double fj = population.get(j).getFitness();
        double fk = population.get(k).getFitness();

        if (fi < fj) {
            if (fi < fk) {
                res.add(population.get(j));
                res.add(population.get(k));
                population.remove(i);
            } else {
                res.add(population.get(j));
                res.add(population.get(i));
                population.remove(k);
            }
        } else {
            if (fj < fk) {
                res.add(population.get(i));
                res.add(population.get(k));
                population.remove(j);
            } else {
                res.add(population.get(j));
                res.add(population.get(i));
                population.remove(k);
            }
        }

        return res;
    }
}