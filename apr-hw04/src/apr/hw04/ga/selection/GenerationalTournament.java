package apr.hw04.ga.selection;

import apr.hw04.ga.chromosome.BinaryChromosome;

import java.util.List;
import java.util.Random;

public class GenerationalTournament implements Selection<BinaryChromosome> {

    private int k;
    private Random rand = new Random();

    public GenerationalTournament(int k) {
        this.k = k;
    }

    @Override
    public List<BinaryChromosome> select(List<BinaryChromosome> population, int n) {
        int i = rand.nextInt(population.size());
        int j = rand.nextInt(population.size());
        while (j == i) {
            j = rand.nextInt(population.size());
        }

        int k = rand.nextInt(population.size());
        while (k == j || k == i) {
            k = rand.nextInt(population.size());
        }

        if (population.get(i).getFitness() < population.get(j).getFitness()) {
            if (population.get(i).getFitness() < population.get(k).getFitness()) {
                return List.of(population.get(i));
            } else {
                return List.of(population.get(k));
            }
        } else {
            if (population.get(j).getFitness() < population.get(k).getFitness()) {
                return List.of(population.get(j));
            } else {
                return List.of(population.get(k));
            }
        }
    }
}