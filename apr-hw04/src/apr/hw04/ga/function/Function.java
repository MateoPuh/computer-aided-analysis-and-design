package apr.hw04.ga.function;

import apr.hw04.ga.mutation.DoubleMutation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Function extends AbstractFunction<List<Double>, Double> {
    Map<List<Double>, Double> values = new HashMap<>();

    @Override
    public abstract Double doFunction(List<Double> x);

    public static Function function1() {
        return new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                double res = 100 * Math.pow(x2 - x1 * x1, 2) + Math.pow(1 - x1, 2);

                values.put(x, res);

                return res;
            }
        };
    }

    public static Function function3() {
        return new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double res = 0;

               for (int i = 0; i < x.size(); i++) {
                   res += Math.pow(x.get(i) - i - 1, 2);
               }

                values.put(x, res);

                return res;
            }
        };
    }

    public static Function function6() {
        return new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double sum = 0;
                for (int i = 0; i < x.size(); i++) {
                    sum += Math.pow(x.get(i), 2);
                }

                double res = 0.5 + (Math.pow(Math.sin(Math.sqrt(sum)), 2) - 0.5) / (1 + 0.001 * sum);

                values.put(x, res);

                return res;
            }
        };
    }

    public static Function function7() {
        return new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double sum = 0;
                for (int i = 0; i < x.size(); i++) {
                    sum += Math.pow(x.get(i), 2);
                }

                double res = Math.pow(sum, 0.25) * (1 + Math.pow(Math.sin(50 * Math.pow(sum, 0.1)) ,2));

                values.put(x, res);

                return res;
            }
        };
    }
}

