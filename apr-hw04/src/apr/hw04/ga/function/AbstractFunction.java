package apr.hw04.ga.function;

public abstract class AbstractFunction<T, V> {
    int counter;

    public abstract V doFunction(T x);

    public int getCounter() {
        return counter;
    }

    public void reset() {
        counter = 0;
    }
}