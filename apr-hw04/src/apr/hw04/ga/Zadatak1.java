package apr.hw04.ga;

import apr.hw04.ga.function.Function;

public class Zadatak1 {

    private static double dg = -50;
    private static double gg = 150;
    private static int size = 200;
    private static int p = 5;
    private static int k = 3;
    private static double pm = 0.2;
    private static int iterLimit = 300000;
    private static boolean print = false;

    public static void main(String[] args) {
        f1();
        f3();
        f6();
        f7();
    }

    private static void fbin(Function f, int dim) {
        Utils.printBinRes(Utils.fbin(f, dim, k, dg, gg, size, dim, p, pm, iterLimit, print), dim, f, dg, gg);
    }

    private static void fdouble(Function f, int dim) {
        Utils.printDoubleRes(Utils.fdouble(f, dim, k, dg, gg, size, dim, p, pm, iterLimit, print), f);
    }

    private static void f1() {
        System.out.println("FUNKCIJA 1");
        Function f1 = Function.function1();

        fbin(f1, 2);

        fdouble(f1, 2);
        System.out.println();
    }

    private static void f3() {
        System.out.println("FUNKCIJA 3");
        Function f3 = Function.function3();

        fbin(f3, 5);

        fdouble(f3, 5);

        System.out.println();
    }

    private static void f6() {
        System.out.println("FUNKCIJA 6");
        Function f6 = Function.function6();

        fbin(f6, 2);

        fdouble(f6, 2);

        System.out.println();
    }

    private static void f7() {
        System.out.println("FUNKCIJA 7");
        Function f7 = Function.function7();

        fbin(f7, 2);

        fdouble(f7, 2);

        System.out.println();
    }
}
