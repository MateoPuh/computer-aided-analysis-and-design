package apr.hw04.ga;

import apr.hw04.ga.chromosome.BinaryChromosome;
import apr.hw04.ga.chromosome.Chromosome;
import apr.hw04.ga.chromosome.ChromosomeUtils;
import apr.hw04.ga.chromosome.DoubleChromosome;
import apr.hw04.ga.function.Function;
import apr.hw04.ga.ga.BinaryEliminationalGA;
import apr.hw04.ga.ga.DoubleEliminationalGA;

import java.util.List;

public class Utils {

    public static BinaryChromosome fbin(Function f, int dim, int k, double dg, double gg, int size, int chromosomeSize, int p, double pm, int iterLimit, boolean print) {
        BinaryEliminationalGA ga = new BinaryEliminationalGA(f, k, dg, gg, size, dim, p, pm, iterLimit, print);

        ga.generateInitialPopulation();
        if (print) {
            System.out.println("Initial population:");
            ga.printPopulation();
            System.out.println();
        }

        BinaryChromosome res = (BinaryChromosome) ga.evolve(5, 0, 1e-6);
        return res;
    }

    public static DoubleChromosome fdouble(Function f, int dim, int k, double dg, double gg, int size, int chromosomeSize, int p, double pm, int iterLimit, boolean print) {
        DoubleEliminationalGA ga = new DoubleEliminationalGA(f, k, dg, gg, size, dim, p, pm, iterLimit, print);

        ga.generateInitialPopulation();
        if (print) {
            System.out.println("Initial population:");
            ga.printPopulation();
            System.out.println();
        }

        DoubleChromosome res = (DoubleChromosome) ga.evolve(5, 0, 1e-6);

        return res;
    }

    public static void printBinRes(BinaryChromosome res, int dim, Function f, double dg, double gg) {
        System.out.println("Binary:");
        System.out.println(res);

        for (int i = 0; i < dim; i++) {
            System.out.print(ChromosomeUtils.getX(res.getbInt().get(i), dg, gg, res.getN()) + " ");
        }
        System.out.println();

        System.out.println("Min value: " + res.getValue(f));
    }

    public static void printDoubleRes(DoubleChromosome res, Function f) {
        System.out.println("Decimal:");
        System.out.println(res.getX());

        System.out.println("Min value: " + res.getValue(f));
    }

    public static double totalFitness(List<Chromosome> population) {
        double total = 0;

        for (Chromosome c : population) {
            total += c.getFitness();
        }

        return total;
    }

    public static Chromosome getBest(List<Chromosome> population) {
        Chromosome best = null;

        for (Chromosome c : population) {
            if (best == null) {
                best = c;
            }

            if (c.getFitness() > best.getFitness()) {
                best = c;
            }
        }

        return best;
    }

    public static double totalPunishment(List<Chromosome> population, double best) {
        double total = 0;

        for (Chromosome c : population) {
            total += best - c.getFitness();
        }

        return total;
    }

    public static String binaryString(int x, int n) {
        String zeros = "0".repeat(n);
        String bitsnlz = Integer.toBinaryString(x );
        String lz = zeros.substring( 0, n - bitsnlz.length() );
        String bitslz = lz.concat(bitsnlz);
        return bitslz;
    }
}
