package com.company.optimisation;

import com.company.function.Function;
import com.company.function.GradientFunction;
import com.company.function.HessianMatrix;
import com.company.matrix.Matrix;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.company.optimisation.Util.*;

public class Optimisation {

    private static int MAX_ITER = 100;
    private static String LIST_FORMAT = "%.9f";

    public static List<Double> gradientDescent(Function f, GradientFunction g, List<Double> x0, double epsilon, boolean firstWay, boolean print) {
        int brIter = 0;

        if (print) System.out.println("============================================================================================================================================================");
        if (print) System.out.println("Gradijentni spust, x0 = " + Util.formatList(x0, LIST_FORMAT)
                + ", epsilon = " + epsilon + ", " + (firstWay ? "w/o optimisation" : "with optimisation"));
        if (print) System.out.println("============================================================================================================================================================");


        if (print) System.out.printf("%5s|%40s|%40s|%40s\n", "i", "x0", "gradijent", "v");
        if (print) System.out.println("============================================================================================================================================================");

        while (true) {
            brIter++;

            if (brIter == MAX_ITER) {
                System.out.println("Postupak divergira u prvih 100 iteracija!");
                break;
            }

            List<Double> gf = g.doFunction(x0);
            List<Double> finalX = x0;

            if (Util.norm(gf) < epsilon) {
                break;
            }

            if (firstWay) {
                List<Double> v = mul(gf, -1);
                x0 = add(x0, v);

                if (print) System.out.printf("%5s|%40s|%40s|%40s\n", brIter, Util.formatList(finalX, LIST_FORMAT),
                        Util.formatList(gf, LIST_FORMAT), Util.formatList(v, LIST_FORMAT));
                if (print) System.out.println("============================================================================================================================================================");
                continue;
            }

            List<Double> v = mul(gf, -1 / Util.norm(gf));

            Function fLambda = new Function() {
                @Override
                public Double doFunction(List<Double> x) {
                    double lambda = x.get(0);
                    return f.doFunction(add(finalX, mul(v, lambda)));
                }
            };

            Interval i = Util.unimodalni(0, 1, fLambda);
            double lambdaMin = Util.zlatniRez(i.a, i.b, 1e-6, fLambda, false);

            x0 = add(x0, mul(v, lambdaMin));

            if (print) System.out.printf("%5s|%40s|%40s|%40s\n", brIter, Util.formatList(finalX, LIST_FORMAT),
                    Util.formatList(gf, LIST_FORMAT), Util.formatList(v, LIST_FORMAT));
            if (print) System.out.println("============================================================================================================================================================");
        }

        return x0;
    }

    public static List<Double> NewtonRaphsonov(Function f, GradientFunction g, HessianMatrix h, List<Double> x0, double epsilon, boolean firstWay, boolean print) {
        int brIter = 0;

        if (print) System.out.println("============================================================================================================================================================");
        if (print) System.out.println("Newton - Raphsonov postupak, x0 = " + Util.formatList(x0, LIST_FORMAT)
                + ", epsilon = " + epsilon + ", " + (firstWay ? "w/o optimisation" : "with optimisation"));
        if (print) System.out.println("============================================================================================================================================================");

        if (print) System.out.printf("%5s|%40s|%40s|%40s\n", "i", "x0", "gradijent", "v");
        if (print) System.out.println("============================================================================================================================================================");

        while (true) {
            brIter++;

            if (brIter == MAX_ITER) {
                System.out.println("Postupak divergira u prvih 100 iteracija!");
                break;
            }

            List<Double> gf = g.doFunction(x0);

            if (Util.norm(gf) < epsilon) {
                break;
            }

            Matrix hf = h.doFunction(x0);
            Matrix hfInv = hf.inverseLUP();
            hfInv.multiplyWithScalar(-1);
            hfInv.mul(new Matrix(gf));

            List<Double> finalX = x0;
            List<Double> v = hfInv.toList();

            if (firstWay) {
                x0 = add(x0, v);

                if (print) System.out.printf("%5s|%40s|%40s|%40s\n", brIter, Util.formatList(finalX, LIST_FORMAT),
                        Util.formatList(gf, LIST_FORMAT), Util.formatList(v, LIST_FORMAT));
                if (print) System.out.println("============================================================================================================================================================");
                continue;
            }

            v = mul(v, Util.norm(v));
            List<Double> finalV = v;

            Function fLambda = new Function() {
                @Override
                public Double doFunction(List<Double> x) {
                    double lambda = x.get(0);
                    return f.doFunction(add(finalX, mul(finalV, lambda)));
                }
            };

            Interval i = Util.unimodalni(0, 1, fLambda);
            double lambdaMin = Util.zlatniRez(i.a, i.b, 1e-6, fLambda, false);

            x0 = add(x0, mul(v, lambdaMin));

            if (print) System.out.printf("%5s|%40s|%40s|%40s\n", brIter, Util.formatList(finalX, LIST_FORMAT),
                    Util.formatList(gf, LIST_FORMAT), Util.formatList(v, LIST_FORMAT));
            if (print) System.out.println("============================================================================================================================================================");
        }

        return x0;
    }

    public static List<Double> box(
            List<Double> x0,
            List<Double> xd,
            List<Double> xg,
            double epsilon,
            Function f,
            List<Function> limitations,
            double alpha,
            boolean print
    ) {
        if (print) System.out.println("Box postupak x0: " + x0 + ", epsilon:" + epsilon + ", alpha:" + alpha);

        int n = x0.size();

        if (x0.size() != xd.size() && x0.size() != xg.size()) {
            throw new IllegalArgumentException();
        }

        for (int i = 0; i < n; i++) {
            if (x0.get(i) < xd.get(i) || x0.get(i) > xg.get(i)) {
                System.out.println("x0 is not in range [xd, xg]");
                return null;
            }
        }

        for (Function af : limitations) {
            if (af.doFunction(x0) < 0) {
                System.out.println("x0 does not satisfy implicit limitation");
                return null;
            }
        }

        List<List<Double>> X = generateNewSet(n, x0, xd, xg, limitations);

        List<Double> xc;

        int l = 0, h = 0, h2 = 0;

        int cnt = 0;

        do {
            if (cnt > MAX_ITER) {
                System.out.println("DIV");
                break;
            }
            l = 0;
            h = 0;
            h2 = 0;

            // find h, l, h2: F(X[h]) = max, F(X[l]) = min, F(X[h2]) = drugi
            double fl = f.doFunction(X.get(0));
            double fh = f.doFunction(X.get(0));
            double fh2 = f.doFunction(X.get(0));

            List<Double> xh = X.get(0);

            for (int i = 0; i < X.size(); i++) {
                double fi = f.doFunction(X.get(i));

                if (fi < fl) {
                    fl = fi;
                    l = i;
                }
                if (fi > fh) {
                    fh = fi;
                    h = i;
                    xh = X.get(i);
                }
            }

            for (int i = 0; i < X.size(); i++) {
                double fi = f.doFunction(X.get(i));

                if (fi < fh && fi > fh2) {
                    fh2 = fi;
                    h2 = i;
                }
            }

            xc = generateCentroid(X, n, h);

            // find xr = Reflection()
            List<Double> xr = reflexion(xc, xh, alpha);

            if (print) System.out.println("\tcentroid: " + xc);
            if (print) System.out.println("\tF(centroid): " + f.doFunction(xc));

            for (int i = 0; i < n; i++) {
                double xri = xr.get(i);

                if (xri < xd.get(i)) {
                    xr.set(i, xd.get(i));
                } else if (xri > xg.get(i)) {
                    xr.set(i, xg.get(i));
                }
            }

            int brIter = 0;
            while (true) {
                List<Double> finalXr = xr;

                if(limitations.stream().noneMatch(it -> it.doFunction(finalXr) < 0)) {
                    break;
                }

                brIter++;
                if (brIter > MAX_ITER) {
//                    System.out.println("divergencija");
                    break;
                }

                xr = mul(add(xr, xc), 1.0/2);
            }

            if (f.doFunction(xr) > fh2) {
                xr = mul(add(xr, xc), 1.0/2);
            }

            X.set(h, xr);
        } while (boxStopCondition(X, xc, f) > epsilon);

        return X.get(l);
    }

    private static List<Double> generateCentroid(List<List<Double>> X, int n) {
        List<Double> xc = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            xc.add(0.0);
        }

        for (int i = 0; i < X.size(); i++) {
            xc = add(xc, X.get(i));
        }

        xc = mul(xc, (double) 1 / (X.size()));

        return xc;
    }

    private static List<Double> generateCentroid(List<List<Double>> X, int n, int h) {
        List<Double> xc = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            xc.add(0.0);
        }

        for (int i = 0; i < X.size(); i++) {
            if (i == h) continue;
            xc = add(xc, X.get(i));
        }

        xc = mul(xc, (double) 1 / (X.size() - 1));

        return xc;
    }

    private static List<List<Double>> generateNewSet(int n, List<Double> x0, List<Double> xd, List<Double> xg, List<Function> limitations) {
        List<List<Double>> X = new ArrayList<>();
        List<Double> xc = x0;

        Random r = new Random();

        for (int t = 0; t < 2 * n; t++) {
            List<Double> xt = generateNewDot(n, r, xd, xg);

            int brIter = 0;
            while (true) {
                List<Double> finalXt = xt;
                if (limitations.stream().noneMatch(it -> it.doFunction(finalXt) < 0)) {
                    break;
                }
                brIter++;
                if (brIter > MAX_ITER) {
//                    System.out.println("divergencija");
                    break;
                }
                xt = mul(add(xt, xc), 1.0/2);
            }

            X.add(xt);

            xc = generateCentroid(X, n);
        }

        return X;
    }

    private static List<Double> generateNewDot(int n, Random r, List<Double> xd, List<Double> xg) {
        List<Double> xt = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            double xdi = xd.get(i);
            double xgi = xg.get(i);
            double xi = xdi + (xgi - xdi) * r.nextDouble();
            xt.add(xi);
        }

        return xt;
    }

    private static List<Double> reflexion(List<Double> xc, List<Double> xh, double alpha) {
        List<Double> xr = mul(xc, 1 + alpha);
        xr = add(xr, mul(xh, -alpha));
        return xr;
    }

    private static double boxStopCondition(List<List<Double>> X, List<Double> xc, Function function) {
        double sum = 0;

        double fxc = function.doFunction(xc);

        for (int i = 0; i < X.size(); i++) {
            sum += Math.pow(function.doFunction(X.get(i)) - fxc, 2);
        }

        sum /= X.size();

        return Math.sqrt(sum);
    }

    public static List<Double> transf(
            List<Double> x0,
            Function f,
            List<Function> h,
            List<Function> g,
            double epsilon,
            double t,
            boolean print
    ) {
        int cnt = 0;

        Function Gfunc = new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                return G(x, 1, g);
            }
        };

        List<Double> dX = new ArrayList<>();
        List<Double> eps = new ArrayList<>();

        for (int i = 0; i < x0.size(); i++) {
            dX.add(0.5);
            eps.add(epsilon);
        }

        x0 = Util.HookeJeeves(x0, dX, eps, Gfunc, false);

        System.out.println(x0 + " " + Gfunc.doFunction(x0));

        while (true) {
            cnt++;
            if (cnt > MAX_ITER) {
                System.out.println("Divergencija");
                break;
            }

            double finalT = t;
            Function u = new Function() {
                @Override
                public Double doFunction(List<Double> x) {
                    return f.doFunction(x) + (1.0 / finalT) * transfIneq(g, x) + finalT * transfEq(h, x);
                }
            };

            List<Double> xNew = Util.HookeJeeves(x0, dX, eps, u, false);

            t *= 10;

            if (print) System.out.println(cnt + ": " + xNew);

            if (distance(x0, xNew) < epsilon) {
                x0 = xNew;
                break;
            }

            x0 = xNew;
        }

        return x0;
    }

    private static double G(List<Double> x, double tpos, List<Function> g) {
        double res = 0;
        for (Function gi : g) {
            double gix = gi.doFunction(x);

            if (gix < 0) {
                res += tpos * gix;
            }
        }

        return -res;
    }

    private static double transfIneq(List<Function> g, List<Double> x) {
        double res = 0;

        for (Function function : g) {
            double gi = function.doFunction(x);
            if (gi < 0) {
                res += Double.POSITIVE_INFINITY;
                continue;
            }
            res -= Math.log(function.doFunction(x));
        }

        return res;
    }

    private static double transfEq(List<Function> h, List<Double> x) {
        double res = 0;

        for (Function function : h) {
            double hi = function.doFunction(x);
            res += hi * hi;
        }

        return res;
    }

    private static double distance(List<Double> x1, List<Double> x2) {
        double res = 0;

        for (int i = 0; i < x1.size(); i++) {
            res += Math.pow(x1.get(i) - x2.get(i), 2);
        }

        return Math.sqrt(res);
    }
}
