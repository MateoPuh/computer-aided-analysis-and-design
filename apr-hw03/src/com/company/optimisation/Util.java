package com.company.optimisation;

import com.company.function.Function;

import java.util.ArrayList;
import java.util.List;

public class Util {

    private static final double k = 0.5 * (Math.sqrt(5) - 1);

    public static double norm(List<Double> vector) {
        double res = 0;

        for (Double d : vector) {
            res += d * d;
        }

        return Math.sqrt(res);
    }

    public static List<Double> mul(List<Double> x1, double lambda) {
        List<Double> res = new ArrayList<>();

        for (int i = 0; i < x1.size(); i++) {
            res.add(x1.get(i) * lambda);
        }

        return res;
    }

    public static List<Double> e(int n, int index) {
        List<Double> res = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            res.add(i == index ? 1.0 : 0.0);
        }

        return res;
    }

    public static List<Double> add(List<Double> x1, List<Double> x2) {
        if (x1.size() != x2.size()) {
            throw new IllegalArgumentException();
        }

        List<Double> res = new ArrayList<>();

        for (int i = 0; i < x1.size(); i++) {
            res.add(x1.get(i) + x2.get(i));
        }

        return res;
    }

    public static boolean error(List<Double> x1, List<Double> x2, List<Double> e) {
        if (x1.size() != x2.size() || x1.size() != e.size()) {
            throw new IllegalArgumentException();
        }

        for (int i = 0; i < x1.size(); i++) {
            if (Math.abs(x1.get(i) - x2.get(i)) > e.get(i)) return true;
        }

        return false;
    }

    public static double zlatniRez(double a, double b, double epsilon, Function f, boolean print) {
        if (print) System.out.println("Zlatni rez: a:" + a + ", b:" + b + ", epsilon:" + epsilon);
        double c = b - k * (b - a);
        double d = a + k * (b - a);
        double fc = f.doFunction(List.of(c));
        double fd = f.doFunction(List.of(d));

        if (print) System.out.println("\ta:" + a + ", c:" + c + ", d:" + d + ", b:" + b
                + ", F(a):" + f.doFunction(List.of(a)) + ", F(c):" + fc + ", F(d):" + fd + ", F(b):" + f.doFunction(List.of(b)));
        while ((b - a) > epsilon) {
            if (fc < fd) {
                b = d;
                d = c;
                c = b - k * (b - a);
                fd = fc;
                fc = f.doFunction(List.of(c));
            } else {
                a = c;
                c = d;
                d = a + k * (b - a);
                fc = fd;
                fd = f.doFunction(List.of(d));
            }
            if (print) System.out.println("\ta:" + a + ", c:" + c + ", d:" + d + ", b:" + b
                    + ", F(a):" + f.doFunction(List.of(a)) + ", F(c):" + fc + ", F(d):" + fd + ", F(b):" + f.doFunction(List.of(b)));
        }

        return (c + d) / 2;
    }

    public static Interval unimodalni(double tocka, double h, Function f) {
        double l = tocka - h;
        double r = tocka + h;

        double m = tocka;
        double fl = f.doFunction(List.of(l));
        double fr = f.doFunction(List.of(r));
        double fm = f.doFunction(List.of(m));

        int step = 1;

        if (fm < fr && fm < fl) {
            return new Interval(l, r);
        } else if (fm > fr) {
            do {
                l = m;
                m = r;
                fm = fr;
                step *= 2;
                r = tocka + h * step;
                fr = f.doFunction(List.of(r));
            } while (fm > fr);
        } else {
            do {
                r = m;
                m = l;
                fm = fl;
                step *= 2;
                l = tocka - h * step;
                fl = f.doFunction(List.of(l));
            } while (fm > fl);
        }

        return new Interval(l, r);
    }

    public static String formatList(List<Double> list, String format) {
        String res = "[";

        res += String.format("%.2f", list.get(0));

        for (int i = 1; i < list.size(); i++) {
            res += String.format(", " + format, list.get(i));
        }

        return res + "]";
    }

    public static List<Double> HookeJeeves(
            List<Double> x0,
            List<Double> dX,
            List<Double> epsilon,
            Function function,
            boolean print) {
        if (print) System.out.println("HookeJeeves: X0: " + x0 + ", epsilon:" + epsilon + ", dX:" + dX);
        List<Double> xp = new ArrayList<>(x0);
        List<Double> xb = new ArrayList<>(x0);
        List<Double> xn;

        if (print) System.out.println("\txb: " + xb + ", xp:" + xp);

        do {
            xn = search(xp, dX, function);

            double fxb = function.doFunction(xb);
            double fxn = function.doFunction(xn);

            if (print) System.out.printf("\txb: " + xb + ", xp: " + xp + ", xn: " + xn + ", dX:" + dX + ", F(xb): %f, F(xp): %f, F(xn): %f\n",
                    fxb, function.doFunction(xp), fxn);

            if (fxn < fxb) {
                xp = add(mul(xn, 2), mul(xb, -1));
                xb = xn;
            } else {
                xp = xb;
                dX = mul(dX, 0.5);
            }
        } while(HookeJeevesError(dX, epsilon));

        return xb;
    }

    private static boolean HookeJeevesError(List<Double> dX, List<Double> epsilon) {
        boolean res = false;

        for (int i = 0; i < dX.size(); i++) {
            if (dX.get(i) > epsilon.get(i)) {
                res = true;
                break;
            }
        }

        return res;
    }

    private static List<Double> search(List<Double> xp, List<Double> dX, Function function) {
        List<Double> x = new ArrayList<>(xp);

        for (int i = 0; i < x.size(); i++) {
            double p = function.doFunction(x);
            double xi = x.get(i) + dX.get(i);
            x.set(i, xi);
            double n = function.doFunction(x);

            if (n > p) {
                xi -= 2 * dX.get(i);
                x.set(i, xi);
                n = function.doFunction(x);

                if (n > p) {
                    xi += dX.get(i);
                    x.set(i, xi);
                }
            }
        }

        return x;
    }

    public static List<Double> simplex(
            List<Double> x0,
            double epsilon,
            Function function,
            double alpha,
            double beta,
            double gama,
            double sigma,
            double step,
            boolean print
    ) {
        if (print) System.out.println("Simplex: X0: " + x0 + ", epsilon:" + epsilon + ", alpha:" + alpha + ", beta:" + beta
                + ", gama:" + gama + ", sigma:" + sigma + ", step:" + step);
        List<List<Double>> X = new ArrayList<>();

        X.add(x0);

        for (int i = 0; i < x0.size(); i++) {
            X.add(add(x0, mul(e(x0.size(), i), step)));
        }

        List<Double> xc;

        int l = 0, h = 0;

        do {
            l = 0;
            h = 0;

            // find h, l: F(X[h]) = max, F(X[l]) = min
            double fl = function.doFunction(X.get(0));
            double fh = function.doFunction(X.get(0));

            List<Double> xh = X.get(0);

            for (int i = 0; i < X.size(); i++) {
                double fi = function.doFunction(X.get(i));

                if (fi < fl) {
                    fl = fi;
                    l = i;
                }
                if (fi > fh) {
                    fh = fi;
                    h = i;
                    xh = X.get(i);
                }
            }

            xc = new ArrayList<>();

            for (int i = 0; i < X.get(0).size(); i++) {
                xc.add(0.0);
            }

            // find centroid xc
            for (int i = 0; i < X.size(); i++) {
                if (i == h) continue;

                xc = add(xc, X.get(i));
            }

            xc = mul(xc, (double) 1 / (X.size() - 1));

            if (print) System.out.println("\tcentroid: " + xc);
            if (print) System.out.println("\tF(centroid): " + function.doFunction(xc));

            // find xr = Reflection()
            List<Double> xr = reflexion(xc, xh, alpha);

            double fr = function.doFunction(xr);

            if (fr < fl) {
                List<Double> xe = expansion(xc, xr, gama);

                if (function.doFunction(xe) < fl) {
                    X.set(h, xe);
                } else {
                    X.set(h, xr);
                }
            } else {
                boolean xrBigger = true;

                for (int i = 0; i < X.size(); i++) {
                    if (i == h) continue;

                    if (fr < function.doFunction(X.get(i))) {
                        xrBigger = false;
                    }
                }

                if (xrBigger) {
                    if (fr < fh) {
                        X.set(h, xr);
                    }
                    List<Double> xk = contraction(xc, xh, beta);

                    if (function.doFunction(xk) < fh) {
                        X.set(h, xk);
                    } else {
                        // move everything toward X[l]
                        for (int i = 0; i < X.size(); i++) {
                            if (i == l) continue;

                            List<Double> xiNew = mul(add(X.get(l), X.get(i)), sigma);

                            X.set(i, xiNew);
                        }
                    }
                } else {
                    X.set(h, xr);
                }
            }
        } while (simplexStopCondition(X, xc, function) > epsilon);

        return X.get(l);
    }

    private static List<Double> reflexion(List<Double> xc, List<Double> xh, double alpha) {
        List<Double> xr = mul(xc, 1 + alpha);
        xr = add(xr, mul(xh, -alpha));
        return xr;
    }

    private static List<Double> expansion(List<Double> xc, List<Double> xr, double gama) {
        List<Double> xe = mul(xc, 1 - gama);
        xe = add(xe, mul(xr, gama));
        return xe;
    }

    private static List<Double> contraction(List<Double> xc, List<Double> xh, double beta) {
        List<Double> xk = mul(xc, 1 - beta);
        xk = add(xk, mul(xh, beta));
        return xk;
    }

    private static double simplexStopCondition(List<List<Double>> X, List<Double> xc, Function function) {
        double sum = 0;

        double fxc = function.doFunction(xc);

        for (int i = 0; i < X.size(); i++) {
            sum += Math.pow(function.doFunction(X.get(i)) - fxc, 2);
        }

        sum /= X.size();

        return Math.sqrt(sum);
    }
}
