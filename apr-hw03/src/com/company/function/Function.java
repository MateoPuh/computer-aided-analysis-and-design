package com.company.function;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Function extends AbstractFunction<List<Double>, Double> {
    Map<List<Double>, Double> values = new HashMap<>();

    @Override
    public abstract Double doFunction(List<Double> x);

    public static Function function1() {
        return new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                double res = 100 * Math.pow(x2 - x1 * x1, 2) + Math.pow(1 - x1, 2);

                values.put(x, res);

                return res;
            }
        };
    }

    public static Function function2() {
        return new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                double res = Math.pow(x1 - 4, 2) + 4 * Math.pow(x2 - 2, 2);

                values.put(x, res);

                return res;
            }
        };
    }

    public static Function function3() {
        return new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double res = 0;

                double x1 = x.get(0);
                double x2 = x.get(1);

                res = Math.pow(x1 - 2, 2) + Math.pow(x2 + 3, 2);

                values.put(x, res);

                return res;
            }
        };
    }

    public static Function function4() {
        return new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                double res = Math.pow(x1 - 3, 2) + x2 * x2;

                values.put(x, res);

                return res;
            }
        };
    }
}

