package com.company.function;

import com.company.matrix.Matrix;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class HessianMatrix extends AbstractFunction<List<Double>, Matrix> {
    Map<List<Double>, Matrix> values = new HashMap<>();

    @Override
    public abstract Matrix doFunction(List<Double> x);

    public static HessianMatrix hessianMatrix1() {
        return new HessianMatrix() {
            @Override
            public Matrix doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                Double[][] m = new Double[2][2];

                m[0][0] = -400 * (x2 - 3 * x1 * x1) + 2;
                m[0][1] = m[1][0] = -400 * x1;
                m[1][1] = 200.0;

                Matrix res = new Matrix(m);

                values.put(x, res);

                return res;
            }
        };
    }

    public static HessianMatrix hessianMatrix2() {
        return new HessianMatrix() {
            @Override
            public Matrix doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                Double[][] m = new Double[2][2];

                m[0][0] = 2.0;
                m[0][1] = m[1][0] = 0.0;
                m[1][1] = 8.0;

                Matrix res = new Matrix(m);

                values.put(x, res);

                return res;
            }
        };
    }

    public static HessianMatrix hessianMatrix3() {
        return new HessianMatrix() {
            @Override
            public Matrix doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                Double[][] m = new Double[2][2];

                m[0][0] = 2.0;
                m[0][1] = m[1][0] = 0.0;
                m[1][1] = 2.0;

                Matrix res = new Matrix(m);

                values.put(x, res);

                return res;
            }
        };
    }

    public static HessianMatrix hessianMatrix4() {
        return new HessianMatrix() {
            @Override
            public Matrix doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                Double[][] m = new Double[2][2];

                m[0][0] = 2.0;
                m[0][1] = m[1][0] = 0.0;
                m[1][1] = 2.0;

                Matrix res = new Matrix(m);

                values.put(x, res);

                return res;
            }
        };
    }
}

