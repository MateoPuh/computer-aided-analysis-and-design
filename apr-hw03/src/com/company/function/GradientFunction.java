package com.company.function;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class GradientFunction extends AbstractFunction<List<Double>, List<Double>> {
    Map<List<Double>, List<Double>> values = new HashMap<>();

    @Override
    public abstract List<Double> doFunction(List<Double> x);

    public static GradientFunction gFunction1() {
        return new GradientFunction() {
            @Override
            public List<Double> doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                double gx1 = -400 * x1 * (x2 - x1 * x1) - 2 * (1 - x1);
                double gx2 = 200 * (x2 - x1 * x1);

                List<Double> res = List.of(gx1, gx2);

                values.put(x, res);

                return res;
            }
        };
    }

    public static GradientFunction gFunction2() {
        return new GradientFunction() {
            @Override
            public List<Double> doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                double gx1 = 2 * (x1 - 4);
                double gx2 = 8 * (x2 - 2);

                List<Double> res = List.of(gx1, gx2);

                values.put(x, res);

                return res;
            }
        };
    }

    public static GradientFunction gFunction3() {
        return new GradientFunction() {
            @Override
            public List<Double> doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                double gx1 = 2 * (x1 - 2);
                double gx2 = 2 * (x2 + 3);

                List<Double> res = List.of(gx1, gx2);

                values.put(x, res);

                return res;
            }
        };
    }

    public static GradientFunction gFunction4() {
        return new GradientFunction() {
            @Override
            public List<Double> doFunction(List<Double> x) {
                if (x.size() != 2) {
                    throw new IllegalArgumentException();
                }

                if (values.containsKey(x)) {
                    return values.get(x);
                }
                counter++;

                double x1 = x.get(0);
                double x2 = x.get(1);

                double gx1 = 2 * (x1 - 3);
                double gx2 = 2 * x2;

                List<Double> res = List.of(gx1, gx2);

                values.put(x, res);

                return res;
            }
        };
    }
}

