package com.company;

import com.company.function.Function;
import com.company.function.GradientFunction;
import com.company.function.HessianMatrix;
import com.company.optimisation.Optimisation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.company.optimisation.Optimisation.*;

public class Main {

    private static final double DEFAULT_EPSILON = 1e-6;

    public static void main(String[] args) {
        zadatak1();
        zadatak2();
        zadatak3();
        zadatak4();
        zadatak5();
    }

    private static void zadatak1() {
        System.out.println("==============================================================================");
        System.out.println("Zadatak 1:");
        System.out.println("==============================================================================");

        Function f3 = Function.function3();
        GradientFunction gf3 = GradientFunction.gFunction3();

        List<Double> x0 = null;
        Double epsilon = null;

        try {
            List<String> lines = Files.readAllLines(Paths.get("zad1param"));
            if (lines.size() > 0) epsilon = Double.parseDouble(lines.get(0).split(" ")[0]);
            if (lines.size() > 1) {
                x0 = new ArrayList<>();
                String firstNum = lines.get(1).split(", ")[0];
                x0.add(Double.parseDouble(firstNum));
                x0.add(Double.parseDouble(lines.get(1).split(", ")[1].split(" ")[0]));
            }
        } catch (IOException e) {
        }

        if (x0 == null) x0 = Arrays.asList(0.0, 0.0);;
        if (epsilon == null) epsilon = DEFAULT_EPSILON;

        List<Double> res = Optimisation.gradientDescent(f3, gf3, x0, epsilon, true, true);
        System.out.println("GradientDescent w/o optimised step result: " + res + "\nfmin: " + f3.doFunction(res)
                + "\nfunction calls : " + f3.getCounter() + "\ngradient function calls : " + gf3.getCounter());

        f3.reset();

        System.out.println();

        List<Double> res2 = Optimisation.gradientDescent(f3, gf3, x0, epsilon, false, true);
        System.out.println("GradientDescent with optimised step result: " + res2 + "\nfmin: " + f3.doFunction(res2)
                + "\nfunction calls : " + f3.getCounter() + "\ngradient function calls : " + gf3.getCounter());

        System.out.println("==============================================================================");
        System.out.println();
    }

    private static void zadatak2() {
        System.out.println("==============================================================================");
        System.out.println("Zadatak 2:");
        System.out.println("==============================================================================");

        List<Double> x0f1 = null;
        List<Double> x0f2 = null;
        Double epsilon = null;
        Boolean firstWay = null;

        try {
            List<String> lines = Files.readAllLines(Paths.get("zad2param"));
            if (lines.size() > 0) epsilon = Double.parseDouble(lines.get(0).split(" ")[0]);
            if (lines.size() > 1) {
                x0f1 = new ArrayList<>();
                String firstNum = lines.get(1).split(", ")[0];
                x0f1.add(Double.parseDouble(firstNum));
                x0f1.add(Double.parseDouble(lines.get(1).split(", ")[1].split(" ")[0]));
            }
            if (lines.size() > 2) {
                x0f2 = new ArrayList<>();
                String firstNum = lines.get(2).split(", ")[0];
                x0f2.add(Double.parseDouble(firstNum));
                x0f2.add(Double.parseDouble(lines.get(2).split(", ")[1].split(" ")[0]));
            }
            if (lines.size() > 3) firstWay = Double.parseDouble(lines.get(3).split(" ")[0]) == 0;
        } catch (IOException e) {
        }

        if (x0f1 == null) x0f1 = Arrays.asList(-1.9, 2.0);
        if (x0f2 == null) x0f2 = Arrays.asList(0.1, 0.3);
        if (epsilon == null) epsilon = DEFAULT_EPSILON;
        if (firstWay == null) firstWay = false;

        Function f1 = Function.function1();
        GradientFunction gf1 = GradientFunction.gFunction1();
        HessianMatrix hf1 = HessianMatrix.hessianMatrix1();

        System.out.println("==============================================================================");
        System.out.println("f1:");
        System.out.println("==============================================================================");

        gradientAndNewton(f1, gf1, hf1, x0f1, epsilon, firstWay);

        System.out.println();

        Function f2 = Function.function2();
        GradientFunction gf2 = GradientFunction.gFunction2();
        HessianMatrix hf2 = HessianMatrix.hessianMatrix2();

        System.out.println("==============================================================================");
        System.out.println("f2:");
        System.out.println("==============================================================================");

        gradientAndNewton(f2, gf2, hf2, x0f2, epsilon, firstWay);

        System.out.println("==============================================================================");
        System.out.println();
    }

    private static void gradientAndNewton(Function f, GradientFunction g, HessianMatrix h, List<Double> x0, double epsilon, boolean firstWay) {
        List<Double> resg = Optimisation.gradientDescent(f, g, x0, epsilon, false, true);
        System.out.println("GradientDescent " + (firstWay ? "w/o optimisation" : "with optimisation") + " step result: " + resg + "\nfmin: " + f.doFunction(resg)
                + "\nfunction calls : " + f.getCounter() + "\ngradient function calls : " + g.getCounter());

        f.reset();
        g.reset();
        h.reset();

        List<Double> resn = Optimisation.NewtonRaphsonov(f, g, h, x0, epsilon, firstWay, true);
        System.out.println("Newton - Raphson " + (firstWay ? "w/o optimisation" : "with optimisation") + "step result: " + resn + "\nfmin: " + f.doFunction(resn)
                + "\nfunction calls : " + f.getCounter() + "\ngradient function calls : " + g.getCounter()
                + "\nhessian matrix calls : " + h.getCounter());
    }

    private static void zadatak3() {
        System.out.println("==============================================================================");
        System.out.println("Zadatak 3:");
        System.out.println("==============================================================================");

        List<Double> x0f1 = null;
        List<Double> x0f2 = null;
        Double epsilon = null;
        Double alpha = null;

        try {
            List<String> lines = Files.readAllLines(Paths.get("zad3param"));
            if (lines.size() > 0) epsilon = Double.parseDouble(lines.get(0).split(" ")[0]);
            if (lines.size() > 1) {
                x0f1 = new ArrayList<>();
                String firstNum = lines.get(1).split(", ")[0];
                x0f1.add(Double.parseDouble(firstNum));
                x0f1.add(Double.parseDouble(lines.get(1).split(", ")[1].split(" ")[0]));
            }
            if (lines.size() > 2) {
                x0f2 = new ArrayList<>();
                String firstNum = lines.get(2).split(", ")[0];
                x0f2.add(Double.parseDouble(firstNum));
                x0f2.add(Double.parseDouble(lines.get(2).split(", ")[1].split(" ")[0]));
            }
            if (lines.size() > 3) alpha = Double.parseDouble(lines.get(3).split(" ")[0]);
        } catch (IOException e) {
        }

        if (x0f1 == null) x0f1 = Arrays.asList(-1.9, 2.0);
        if (x0f2 == null) x0f2 = Arrays.asList(0.1, 0.3);
        if (epsilon == null) epsilon = DEFAULT_EPSILON;
        if (alpha == null) alpha = 1.3;

        List<Function> limitations = new ArrayList<>();

        Function lim1 = new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                double x1 = x.get(0);
                double x2 = x.get(1);

                return x2 - x1;
            }
        };

        Function lim2 = new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                double x1 = x.get(0);

                return 2 - x1;
            }
        };

        limitations.add(lim1);
        limitations.add(lim2);

        List<Double> xd = List.of(-100.0, -100.0);
        List<Double> xg = List.of(100.0, 100.0);

        Function f1 = Function.function1();

        System.out.println("==============================================================================");
        System.out.println("f1:");
        System.out.println("==============================================================================");

        box(f1, x0f1, xd, xg, limitations, alpha, epsilon);

        System.out.println();

        Function f2 = Function.function2();

        System.out.println("==============================================================================");
        System.out.println("f2:");
        System.out.println("==============================================================================");

        box(f2, x0f2, xd, xg, limitations, alpha, epsilon);

        System.out.println("==============================================================================");
        System.out.println();
    }

    private static void box(Function f, List<Double> x0, List<Double> xd, List<Double> xg, List<Function> limitations, double alpha, double epsilon) {
        List<Double> res = Optimisation.box(x0, xd, xg, epsilon, f, limitations, alpha, true);

        System.out.println("Box postupak result: " + res + "\nfmin: " + f.doFunction(res)
                + "\nfunction calls : " + f.getCounter());
    }

    private static void zadatak4() {
        System.out.println("==============================================================================");
        System.out.println("Zadatak 4:");
        System.out.println("==============================================================================");

        List<Double> x0f1 = null;
        List<Double> x0f2 = null;
        Double epsilon = null;
        Double t = null;

        try {
            List<String> lines = Files.readAllLines(Paths.get("zad4param"));
            if (lines.size() > 0) epsilon = Double.parseDouble(lines.get(0).split(" ")[0]);
            if (lines.size() > 1) {
                x0f1 = new ArrayList<>();
                String firstNum = lines.get(1).split(", ")[0];
                x0f1.add(Double.parseDouble(firstNum));
                x0f1.add(Double.parseDouble(lines.get(1).split(", ")[1].split(" ")[0]));
            }
            if (lines.size() > 2) {
                x0f2 = new ArrayList<>();
                String firstNum = lines.get(2).split(", ")[0];
                x0f2.add(Double.parseDouble(firstNum));
                x0f2.add(Double.parseDouble(lines.get(2).split(", ")[1].split(" ")[0]));
            }
            if (lines.size() > 3) t = Double.parseDouble(lines.get(3).split(" ")[0]);
        } catch (IOException e) {
        }

        if (x0f1 == null) x0f1 = Arrays.asList(-1.9, 2.0);
        if (x0f2 == null) x0f2 = Arrays.asList(0.1, 0.3);
        if (epsilon == null) epsilon = DEFAULT_EPSILON;
        if (t == null) t = 1.0;

        List<Function> limitations = new ArrayList<>();

        Function lim1 = new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                double x1 = x.get(0);
                double x2 = x.get(1);

                return x2 - x1;
            }
        };

        Function lim2 = new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                double x1 = x.get(0);

                return 2 - x1;
            }
        };

        limitations.add(lim1);
        limitations.add(lim2);

        Function f1 = Function.function1();

        System.out.println("==============================================================================");
        System.out.println("f1:");
        System.out.println("==============================================================================");

        List<Double> res1 = transf(x0f1, f1, new ArrayList<>(), limitations, epsilon, 1, true);

        System.out.println("Postupak transformacije result: " + res1 + "\nfmin: " + f1.doFunction(res1)
                + "\nfunction calls : " + f1.getCounter());

        System.out.println();

        Function f2 = Function.function2();

        System.out.println("==============================================================================");
        System.out.println("f2:");
        System.out.println("==============================================================================");

        List<Double> res2 = transf(x0f2, f2, new ArrayList<>(), limitations, epsilon, 1, true);

        System.out.println("Postupak transformacije result: " + res2 + "\nfmin: " + f2.doFunction(res2)
                + "\nfunction calls : " + f2.getCounter());

        System.out.println("==============================================================================");
        System.out.println();
    }

    private static void zadatak5() {
        System.out.println("==============================================================================");
        System.out.println("Zadatak 5:");
        System.out.println("==============================================================================");

        List<Double> x0 = null;
        Double epsilon = null;
        Double t = null;

        try {
            List<String> lines = Files.readAllLines(Paths.get("zad5param"));
            if (lines.size() > 0) epsilon = Double.parseDouble(lines.get(0).split(" ")[0]);
            if (lines.size() > 1) {
                x0 = new ArrayList<>();
                String firstNum = lines.get(1).split(", ")[0];
                x0.add(Double.parseDouble(firstNum));
                x0.add(Double.parseDouble(lines.get(1).split(", ")[1].split(" ")[0]));
            }
            if (lines.size() > 2) t = Double.parseDouble(lines.get(2).split(" ")[0]);
        } catch (IOException e) {
        }

        if (x0 == null) x0 = Arrays.asList(5.0, 5.0);
        if (epsilon == null) epsilon = DEFAULT_EPSILON;
        if (t == null) t = 1.0;

        List<Function> limitations = new ArrayList<>();

        Function lim1 = new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                double x1 = x.get(0);
                double x2 = x.get(1);

                return 3 - x2 - x1;
            }
        };

        Function lim2 = new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                double x1 = x.get(0);
                double x2 = x.get(1);

                return 3 + 1.5  * x1 - x2;
            }
        };

        limitations.add(lim1);
        limitations.add(lim2);

        Function lim3 = new Function() {
            @Override
            public Double doFunction(List<Double> x) {
                double x2 = x.get(1);

                return x2 - 1;
            }
        };

        List<Function> hlimitations = new ArrayList<>();

        hlimitations.add(lim3);

        Function f4 = Function.function4();

        System.out.println("==============================================================================");
        System.out.println("f4:");
        System.out.println("==============================================================================");

        List<Double> res1 = transf(x0, f4, hlimitations, limitations, epsilon, 1, true);

        System.out.println("Postupak transformacije result: " + res1 + "\nfmin: " + f4.doFunction(res1)
                + "\nfunction calls : " + f4.getCounter());
        System.out.println("==============================================================================");
    }
}
