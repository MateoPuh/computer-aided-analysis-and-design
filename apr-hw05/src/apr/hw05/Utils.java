package apr.hw05;

import apr.hw05.function.VectorFunction;
import apr.hw05.procedure.Procedure;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static List<Double> add(List<Double> l1, List<Double> l2) {
        List<Double> res = new ArrayList<>();

        for (int i = 0; i < l1.size(); i++) {
            res.add(l1.get(i) + l2.get(i));
        }

        return res;
    }

    public static List<Double> mul(List<Double> l1, double m) {
        List<Double> res = new ArrayList<>();

        for (int i = 0; i < l1.size(); i++) {
            res.add(l1.get(i) * m);
        }

        return res;
    }

    public static Pair<List<Double>, List<List<Double>>> run(Procedure procedure, double T, double tMax, List<Double> x0, VectorFunction real, int num) {
        List<Double> absDiffSum = new ArrayList<>();
        List<List<Double>> values = new ArrayList<>();

        values.add(x0);

        absDiffSum.add(0.0);
        absDiffSum.add(0.0);

        List<Double> xk = x0;
        int i = 0;
        for (double t = 0; t <= tMax; t += T) {
            List<Double> xkplus1 = procedure.xkplus1(xk, t);
            xk = xkplus1;

            values.add(xk);

            i++;

            List<Double> realValue = real.doFunction(new Pair<>(t, x0));

            absDiffSum.set(0, absDiffSum.get(0) + Math.abs(realValue.get(0) - xkplus1.get(0)));
            absDiffSum.set(1, absDiffSum.get(1) + Math.abs(realValue.get(1) - xkplus1.get(1)));

            if (i % num == 0) System.out.println(xk + " " + realValue);
        }

        return new Pair(absDiffSum, values);
    }
}
