package apr.hw05;

import apr.hw05.matrix.Matrix;
import apr.hw05.procedure.*;
import javafx.util.Pair;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static apr.hw05.function.VectorFunction.f1;

public class Zadatak2 {

    public static int NUM = 1;
    public static int PECE_NUM = 20;

    private static double T = 0.1;
    private static double tMax = 1;

    public static void main(String[] args) throws IOException {
        Matrix A = new Matrix("zad2/A");
        List<Double> x0 = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get("zad2/x0"))) {
            stream.forEach(it -> x0.add(Double.parseDouble(it)));
        }

        System.out.println("Izravni Euler:");

        IzravniEuler izravniEuler = new IzravniEuler(A, null, null, T);
        Pair<List<Double>, List<List<Double>>> res = Utils.run(izravniEuler, T, tMax, x0, f1, NUM);

        System.out.println();
        System.out.println("Absolute difference error sum:");
        System.out.println(res.getKey());
        System.out.println();

        System.out.println("Obrnuti Euler:");

        ObrnutiEuler obrnutiEuler = new ObrnutiEuler(A, null, null, T);
        res = Utils.run(obrnutiEuler, T, tMax, x0, f1, NUM);

        System.out.println();
        System.out.println("Absolute difference error sum:");
        System.out.println(res.getKey());
        System.out.println();

        System.out.println("Trapezni:");

        Trapez trapez = new Trapez(A, null, null, T);
        res = Utils.run(trapez, T, tMax, x0, f1, NUM);

        System.out.println();
        System.out.println("Absolute difference error sum:");
        System.out.println(res.getKey());
        System.out.println();

        System.out.println("Runge Kutta:");

        RungeKutta rungeKutta = new RungeKutta(A, null, null, T);
        res = Utils.run(rungeKutta, T, tMax, x0, f1, NUM);

        System.out.println();
        System.out.println("Absolute difference error sum:");
        System.out.println(res.getKey());

        System.out.println();

        System.out.println("PECE:");

        PECE pece = new PECE(A, null, null, T, izravniEuler, PECE_NUM);
        res = Utils.run(pece, T, tMax, x0, f1, NUM);

        System.out.println();
        System.out.println("Absolute difference error sum:");
        System.out.println(res.getKey());
        System.out.println();

        System.out.println("PECE2:");

        PECE2 pece2 = new PECE2(A, null, null, T, izravniEuler, PECE_NUM);
        res = Utils.run(pece2, T, tMax, x0, f1, NUM);

        System.out.println();
        System.out.println("Absolute difference error sum:");
        System.out.println(res.getKey());
        System.out.println();
    }
}
