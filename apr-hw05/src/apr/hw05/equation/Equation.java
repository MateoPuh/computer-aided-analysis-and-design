package apr.hw05.equation;

import apr.hw05.Utils;
import apr.hw05.function.R;
import apr.hw05.matrix.Matrix;

import java.util.List;

public class Equation {

    public static List<Double> doFunction(Matrix A, List<Double> x, Matrix B, R r, double t) {
        return Utils.add(A.multiply(x), B.multiply(r.doFunction(t)));
    }
}
