package apr.hw05.function;

import java.util.List;

public abstract class R extends AbstractFunction<Double, List<Double>> {

    @Override
    public abstract List<Double> doFunction(Double t);

    public static R empty = new R() {
        @Override
        public List<Double> doFunction(Double t) {
            return List.of(0.0, 0.0);
        }
    };

    public static R r3 = new R() {
        @Override
        public List<Double> doFunction(Double t) {
            return List.of(1.0, 1.0);
        }
    };

    public static R r4 = new R() {
        @Override
        public List<Double> doFunction(Double t) {
            return List.of(t, t);
        }
    };
}

