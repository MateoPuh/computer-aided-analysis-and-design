package apr.hw05.function;

import javafx.util.Pair;

import java.util.List;

public abstract class VectorFunction extends AbstractFunction<Pair<Double, List<Double>>, List<Double>> {

    @Override
    public abstract List<Double> doFunction(Pair<Double, List<Double>> t);

    public static VectorFunction f1 = new VectorFunction() {
        @Override
        public List<Double> doFunction(Pair<Double, List<Double>> t) {
            double x10 = t.getValue().get(0);
            double x20 = t.getValue().get(0);

            return List.of(x10 * Math.cos(t.getKey()) + x20 * Math.sin(t.getKey()), x20 * Math.cos(t.getKey()) - x10 * Math.sin(t.getKey()));
        }
    };
}

