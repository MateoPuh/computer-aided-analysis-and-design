package apr.hw05.procedure;

import apr.hw05.Utils;
import apr.hw05.equation.Equation;
import apr.hw05.function.R;
import apr.hw05.matrix.Matrix;

import java.util.List;

public class Trapez extends Procedure {

    public Trapez(Matrix a, Matrix b, R r, double t) {
        super(a, b, r, t);
    }

    @Override
    public List<Double> xkplus1(List<Double> xk, double tk) {
        Matrix U = Matrix.eye(xk.size());
        Matrix H = U.add(A.multiplyWithScalar(-T/2));
        H = H.inverse(null);
        Matrix R = H.mulEquals(U.add(A.multiplyWithScalar(T/2)));

        double tkplus1 = tk + T;

        Matrix S = H.mulEquals(B.multiplyWithScalar(T/2));


        R newR = new R() {
            @Override
            public List<Double> doFunction(Double t) {
                return Utils.add(r.doFunction(t), r.doFunction(t + T));
            }
        };

        return Equation.doFunction(R, xk, S, newR, tk);
    }
}
