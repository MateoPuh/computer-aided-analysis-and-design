package apr.hw05.procedure;

import apr.hw05.Utils;
import apr.hw05.equation.Equation;
import apr.hw05.function.R;
import apr.hw05.matrix.Matrix;

import java.util.List;

public class RungeKutta extends Procedure {

    public RungeKutta(Matrix a, Matrix b, R r, double t) {
        super(a, b, r, t);
    }

    @Override
    public List<Double> xkplus1(List<Double> xk, double tk) {
        return Utils.add(xk, Utils.mul(
                Utils.add(
                        Utils.add(m1(xk, tk), Utils.mul(m2(xk, tk), 2)),
                        Utils.add(Utils.mul(m3(xk, tk), 2), m4(xk, tk))),
                T/6));
    }

    private List<Double> m1(List<Double> xk, double tk) {
        return Equation.doFunction(A, xk, B, r, tk);
    }

    private List<Double> m2(List<Double> xk, double tk) {
        return Equation.doFunction(A, Utils.add(xk, Utils.mul(m1(xk, tk), T/2)), B, r, tk + T/2);
    }

    private List<Double> m3(List<Double> xk, double tk) {
        return Equation.doFunction(A, Utils.add(xk, Utils.mul(m2(xk, tk), T/2)), B, r, tk + T/2);
    }

    private List<Double> m4(List<Double> xk, double tk) {
        return Equation.doFunction(A, Utils.add(xk, Utils.mul(m3(xk, tk), T)), B, r, tk + T);
    }
}
