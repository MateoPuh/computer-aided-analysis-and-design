package apr.hw05.procedure;

import apr.hw05.equation.Equation;
import apr.hw05.function.R;
import apr.hw05.matrix.Matrix;

import java.util.List;

public class ObrnutiEuler extends Procedure {

    public ObrnutiEuler(Matrix a, Matrix b, R r, double t) {
        super(a, b, r, t);
    }

    @Override
    public List<Double> xkplus1(List<Double> xk, double tk) {
        Matrix U = Matrix.eye(xk.size());
        Matrix P = U.add(A.multiplyWithScalar(-T));
        P = P.inverse(null);

        double tkplus1 = tk + T;

        Matrix Q = P.mulEquals(B.multiplyWithScalar(T));

        return Equation.doFunction(P, xk, Q, r, tkplus1);
    }
}
