package apr.hw05.procedure;

import apr.hw05.equation.Equation;
import apr.hw05.function.R;
import apr.hw05.matrix.Matrix;

import java.util.List;

public class IzravniEuler extends Procedure {

    public IzravniEuler(Matrix A, Matrix B, R r, double T) {
        super(A, B, r, T);
    }

    @Override
    public List<Double> xkplus1(List<Double> xk, double tk) {
        Matrix U = Matrix.eye(xk.size());
        Matrix h = A.multiplyWithScalar(T);
        Matrix M = U.add(h);

        Matrix N = B.multiplyWithScalar(T);

        return Equation.doFunction(M, xk, N, r, tk);
    }
}
