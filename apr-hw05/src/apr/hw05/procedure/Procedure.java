package apr.hw05.procedure;

import apr.hw05.function.R;
import apr.hw05.matrix.Matrix;

import java.util.List;

public abstract class Procedure {

    protected Matrix A;
    protected Matrix B;
    protected R r;
    protected double T;

    public Procedure(Matrix A, Matrix B, R r, double T) {
        if (B == null) {
            Double[][] content = new Double[A.getRows()][A.getColumns()];

            for (int i = 0; i < A.getRows(); i++) {
                for (int j = 0; j < A.getColumns(); j++) {
                    content[i][j] = 0.0;
                }
            }

            B = new Matrix(content);
        }

        if (r == null) r = R.empty;

        this.A = A;
        this.B = B;
        this.r = r;
        this.T = T;
    }

    public abstract List<Double> xkplus1(List<Double> xk, double tk);

    public Matrix getA() {
        return A;
    }

    public void setA(Matrix a) {
        A = a;
    }

    public Matrix getB() {
        return B;
    }

    public void setB(Matrix b) {
        B = b;
    }

    public R getR() {
        return r;
    }

    public void setR(R r) {
        this.r = r;
    }

    public double getT() {
        return T;
    }

    public void setT(double t) {
        T = t;
    }

}
