package apr.hw05.procedure;

import apr.hw05.Utils;
import apr.hw05.equation.Equation;
import apr.hw05.function.R;
import apr.hw05.matrix.Matrix;

import java.util.List;

public class PECE extends Procedure {

    private Procedure explicit;
    private int num;

    public PECE(Matrix A, Matrix B, R r, double T, Procedure explicit, int num) {
        super(A, B, r, T);
        this.explicit = explicit;
        this.num = num;
    }

    @Override
    public List<Double> xkplus1(List<Double> xk, double tk) {
        // predikcija
        List<Double> xkplus1 = explicit.xkplus1(xk, tk);

        double tkplus1 = tk + T;

        for (int i = 0; i < num; i++) {
            List<Double> xkplus1Next =
                    Utils.add(xk,
                            Utils.mul(
                                Utils.add(
                                        Equation.doFunction(A, xk, B, r, tk),
                                        Equation.doFunction(A, xkplus1, B, r, tkplus1)
                                ), T/2));

            xkplus1 = xkplus1Next;
        }

        return xkplus1;
    }
}
