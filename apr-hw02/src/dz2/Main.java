package dz2;

import dz2.function.Function;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static dz2.Optimisation.*;

public class Main {

    private static final double DEFAULT_EPSILON = 1e-6;
    private static final double DEFAULT_ALPHA = 1;
    private static final double DEFAULT_BETA = 0.5;
    private static final double DEFAULT_GAMA = 2;
    private static final double DEFAULT_SIGMA = 0.5;
    private static final double DEFAULT_DX = 0.5;

    public static void main(String[] args) {
        zadatak1();
        zadatak2();
        zadatak3();
        zadatak4();
        zadatak5();
    }

    private static void zadatak1() {
        System.out.println("==============================================================================");
        System.out.println("Zadatak 1:");
        System.out.println("==============================================================================");

        Double x0 = null;
        Double epsilon = null;
        Double alfa = null;
        Double beta = null;
        Double gama = null;
        Double sigma = null;
        Double step = null;
        Double dX = null;

        try {
            List<String> lines = Files.readAllLines(Paths.get("zad1param"));
            if (lines.size() > 0) epsilon = Double.parseDouble(lines.get(0).split(" ")[0]);
            if (lines.size() > 1) x0 = Double.parseDouble(lines.get(1).split(" ")[0]);
            if (lines.size() > 2) alfa = Double.parseDouble(lines.get(2).split(" ")[0]);
            if (lines.size() > 3) beta = Double.parseDouble(lines.get(3).split(" ")[0]);
            if (lines.size() > 4) gama = Double.parseDouble(lines.get(4).split(" ")[0]);
            if (lines.size() > 5) sigma = Double.parseDouble(lines.get(5).split(" ")[0]);
            if (lines.size() > 6) step = Double.parseDouble(lines.get(6).split(" ")[0]);
            if (lines.size() > 7) dX = Double.parseDouble(lines.get(7).split(" ")[0]);
        } catch (IOException e) {
        }

        if (x0 == null) x0 = 10.0;
        if (epsilon == null) epsilon = DEFAULT_EPSILON;
        if (alfa == null) alfa = DEFAULT_ALPHA;
        if (beta == null) beta = DEFAULT_BETA;
        if (gama == null) gama = DEFAULT_GAMA;
        if (sigma == null) sigma = DEFAULT_SIGMA;
        if (step == null) step = 1.0;
        if (dX == null) dX = DEFAULT_DX;

        Function f3 = Function.function3zad1();
        Interval interval = unimodalni(x0, 1, f3);
        double res = zlatniRez(interval.a, interval.b, epsilon, f3, true);
        System.out.println("Zlatni rez result:" + res + "\nfmin: " + f3.doFunction(List.of(res)) + "\ncounter:" + f3.getCounter());
        System.out.println();

        f3.reset();

        List<Double> x0list = new ArrayList<>();
        x0list.add(x0);
        List<Double> epsilonList = new ArrayList<>();
        epsilonList.add(epsilon);

        List<Double> res2 = trazenjePoKoordinatnimOsima(x0list, epsilonList, f3, true);
        System.out.println("Trazenje po koordinatnim osima result:" + res2 + "\nfmin: " + f3.doFunction(res2) + "\ncounter " + f3.getCounter());
        System.out.println();

        f3.reset();

        List<Double> res3 = simplex(x0list, epsilon, f3, alfa, beta, gama, sigma, step, true);
        System.out.println("Simplex result:" + res3 + "\nfmin: " + f3.doFunction(res3) + "\ncounter " + f3.getCounter());
        System.out.println();

        f3.reset();

        List<Double> dXlist = new ArrayList<>();
        for (Double xi : x0list) {
            dXlist.add(dX);
        }

        List<Double> res4 = HookeJeeves(x0list, dXlist, epsilonList, f3, true);
        System.out.println("HookeJevees result:" + res4 + "\nfmin: " + f3.doFunction(res4) + "\ncounter " + f3.getCounter());
        System.out.println("==============================================================================");
        System.out.println();
    }

    private static void zadatak2() {
        System.out.println("==============================================================================");
        System.out.println("Zadatak 2:");
        System.out.println("==============================================================================");

        Double epsilon = null;
        Double alfa = null;
        Double beta = null;
        Double gama = null;
        Double sigma = null;
        Double step = null;
        Double dX = null;

        try {
            List<String> lines = Files.readAllLines(Paths.get("zad2param"));
            if (lines.size() > 0) epsilon = Double.parseDouble(lines.get(0).split(" ")[0]);
            if (lines.size() > 1) alfa = Double.parseDouble(lines.get(1).split(" ")[0]);
            if (lines.size() > 2) beta = Double.parseDouble(lines.get(2).split(" ")[0]);
            if (lines.size() > 3) gama = Double.parseDouble(lines.get(3).split(" ")[0]);
            if (lines.size() > 4) sigma = Double.parseDouble(lines.get(4).split(" ")[0]);
            if (lines.size() > 5) step = Double.parseDouble(lines.get(5).split(" ")[0]);
            if (lines.size() > 6) dX = Double.parseDouble(lines.get(6).split(" ")[0]);
        } catch (IOException e) {
        }

        if (epsilon == null) epsilon = DEFAULT_EPSILON;
        if (alfa == null) alfa = DEFAULT_ALPHA;
        if (beta == null) beta = DEFAULT_BETA;
        if (gama == null) gama = DEFAULT_GAMA;
        if (sigma == null) sigma = DEFAULT_SIGMA;
        if (step == null) step = 1.0;
        if (dX == null) dX = DEFAULT_DX;

        System.out.printf("%5s|%40s|%40s|%40s\n", " ", "trazenje po koord", "simplex", "Hoove Jeeves");
        System.out.println("============================================================================================================================================================");

        f1zad2(epsilon, alfa, beta, gama, sigma, step, dX);
        f2zad2(epsilon, alfa, beta, gama, sigma, step, dX);
        f3zad2(epsilon, alfa, beta, gama, sigma, step, dX);
        f4zad2(epsilon, alfa, beta, gama, sigma, step, dX);
        System.out.println("==============================================================================");
        System.out.println();
    }

    public static void f1zad2(double epsilon, double alfa, double beta, double gama, double sigma, double step, double dX) {
        List<Double> x0f1 = Arrays.asList(-1.9, 2.0);
        List<Double> epsilonList = Arrays.asList(epsilon, epsilon);
        Function f1 = Function.function1();
        List<Double> res1 = trazenjePoKoordinatnimOsima(x0f1, epsilonList, f1, false);

        f1.reset();

        List<Double> res2 = simplex(x0f1, epsilon, f1, alfa, beta, gama, sigma, step, false);

        f1.reset();

        List<Double> dXlist = x0f1.stream().map(it -> dX).collect(Collectors.toList());
        List<Double> res3 = HookeJeeves(x0f1, dXlist, epsilonList, f1, false);

        System.out.printf("%5s|%40s|%40s|%40s\n", "f1", formatList(res1), formatList(res2), formatList(res3));
        System.out.printf("%5s|%40s|%40s|%40s\n", " ", f1.getCounter(), f1.getCounter(), f1.getCounter());
        System.out.println("============================================================================================================================================================");
    }

    public static void f2zad2(double epsilon, double alfa, double beta, double gama, double sigma, double step, double dX) {
        List<Double> x0f1 = Arrays.asList(0.1, 0.3);
        List<Double> epsilonList = Arrays.asList(epsilon, epsilon);
        Function f2 = Function.function2();
        List<Double> res1 = trazenjePoKoordinatnimOsima(x0f1, epsilonList, f2, false);

        f2.reset();

        List<Double> res2 = simplex(x0f1, epsilon, f2, alfa, beta, gama, sigma, step, false);

        f2.reset();

        List<Double> dXlist = x0f1.stream().map(it -> dX).collect(Collectors.toList());
        List<Double> res3 = HookeJeeves(x0f1, dXlist, epsilonList, f2, false);

        System.out.printf("%5s|%40s|%40s|%40s\n", "f2",formatList(res1), formatList(res2), formatList(res3));
        System.out.printf("%5s|%40s|%40s|%40s\n", " ", f2.getCounter(), f2.getCounter(), f2.getCounter());
        System.out.println("============================================================================================================================================================");
    }

    public static void f3zad2(double epsilon, double alfa, double beta, double gama, double sigma, double step, double dX) {
        List<Double> x0f1 = Arrays.asList(0.0, 0.0, 0.0, 0.0, 0.0);
        List<Double> epsilonList = Arrays.asList(epsilon, epsilon, epsilon, epsilon, epsilon);
        Function f3 = Function.function3();
        List<Double> res1 = trazenjePoKoordinatnimOsima(x0f1, epsilonList, f3, false);

        f3.reset();

        List<Double> res2 = simplex(x0f1, epsilon, f3, alfa, beta, gama, sigma, step, false);

        f3.reset();

        List<Double> dXlist = x0f1.stream().map(it -> dX).collect(Collectors.toList());
        List<Double> res3 = HookeJeeves(x0f1, dXlist, epsilonList, f3, false);

        System.out.printf("%5s|%40s|%40s|%40s\n", "f2", formatList(res1), formatList(res2), formatList(res3));
        System.out.printf("%5s|%40s|%40s|%40s\n", " ", f3.getCounter(), f3.getCounter(), f3.getCounter());
        System.out.println("============================================================================================================================================================");
    }

    public static void f4zad2(double epsilon, double alfa, double beta, double gama, double sigma, double step, double dX) {
        List<Double> x0f1 = Arrays.asList(5.1, 1.1);
        List<Double> epsilonList = Arrays.asList(epsilon, epsilon);
        Function f4 = Function.function4();
        List<Double> res1 = trazenjePoKoordinatnimOsima(x0f1, epsilonList, f4, false);

        f4.reset();

        List<Double> res2 = simplex(x0f1, epsilon, f4, alfa, beta, gama, sigma, step, false);

        f4.reset();

        List<Double> dXlist = x0f1.stream().map(it -> dX).collect(Collectors.toList());
        List<Double> res3 = HookeJeeves(x0f1, dXlist, epsilonList, f4, false);

        System.out.printf("%5s|%40s|%40s|%40s\n", "f4", formatList(res1), formatList(res2), formatList(res3));
        System.out.printf("%5s|%40s|%40s|%40s\n", " ", f4.getCounter(), f4.getCounter(), f4.getCounter());
        System.out.println("============================================================================================================================================================");
    }

    private static void zadatak3() {
        System.out.println("==============================================================================");
        System.out.println("Zadatak 3:");
        System.out.println("==============================================================================");

        List<Double> x0 = null;
        Double epsilon = null;
        Double alfa = null;
        Double beta = null;
        Double gama = null;
        Double sigma = null;
        Double step = null;
        Double dX = null;

        try {
            List<String> lines = Files.readAllLines(Paths.get("zad3param"));
            if (lines.size() > 0) epsilon = Double.parseDouble(lines.get(0).split(" ")[0]);
            if (lines.size() > 1) {
                x0 = new ArrayList<>();
                String firstNum = lines.get(1).split(", ")[0];
                x0.add(Double.parseDouble(firstNum));
                x0.add(Double.parseDouble(lines.get(1).split(", ")[1].split(" ")[0]));
            }
            if (lines.size() > 2) alfa = Double.parseDouble(lines.get(2).split(" ")[0]);
            if (lines.size() > 3) beta = Double.parseDouble(lines.get(3).split(" ")[0]);
            if (lines.size() > 4) gama = Double.parseDouble(lines.get(4).split(" ")[0]);
            if (lines.size() > 5) sigma = Double.parseDouble(lines.get(5).split(" ")[0]);
            if (lines.size() > 6) step = Double.parseDouble(lines.get(6).split(" ")[0]);
            if (lines.size() > 7) dX = Double.parseDouble(lines.get(7).split(" ")[0]);
        } catch (IOException e) {
        }

        if (x0 == null) x0 = Arrays.asList(5.0, 5.0);;
        if (epsilon == null) epsilon = DEFAULT_EPSILON;
        if (alfa == null) alfa = DEFAULT_ALPHA;
        if (beta == null) beta = DEFAULT_BETA;
        if (gama == null) gama = DEFAULT_GAMA;
        if (sigma == null) sigma = DEFAULT_SIGMA;
        if (step == null) step = 1.0;
        if (dX == null) dX = DEFAULT_DX;

        List<Double> epsilonList = Arrays.asList(epsilon, epsilon);

        Function f4 = Function.function4();
        List<Double> res1 = simplex(x0, epsilon, f4, alfa, beta, gama, sigma, step, false);

        System.out.println("Simplex result:" + res1 + "\nfmin: " + f4.doFunction(res1) + "\ncounter " + f4.getCounter());
        System.out.println();

        f4.reset();

        Double finalDX = dX;
        List<Double> dXlist = x0.stream().map(it -> finalDX).collect(Collectors.toList());
        List<Double> res2 = HookeJeeves(x0, dXlist, epsilonList, f4, false);

        System.out.println("HJ result:" + res2 + "\nfmin: " + f4.doFunction(res2) + "\ncounter " + f4.getCounter());

        System.out.println("==============================================================================");
        System.out.println();
    }

    private static void zadatak4() {
        System.out.println("==============================================================================");
        System.out.println("Zadatak 4:");
        System.out.println("==============================================================================");

        List<Double> x01 = null;
        List<Double> x02 = null;
        Double epsilon = null;
        Double alfa = null;
        Double beta = null;
        Double gama = null;
        Double sigma = null;
        Double step = null;
        Double dX = null;

        try {
            List<String> lines = Files.readAllLines(Paths.get("zad4param"));
            if (lines.size() > 0) epsilon = Double.parseDouble(lines.get(0).split(" ")[0]);
            if (lines.size() > 1) {
                x01 = new ArrayList<>();
                String firstNum = lines.get(1).split(", ")[0];
                x01.add(Double.parseDouble(firstNum));
                x01.add(Double.parseDouble(lines.get(1).split(", ")[1].split(" ")[0]));
            }
            if (lines.size() > 2) {
                x02 = new ArrayList<>();
                String firstNum = lines.get(2).split(", ")[0];
                x02.add(Double.parseDouble(firstNum));
                x02.add(Double.parseDouble(lines.get(2).split(", ")[1].split(" ")[0]));
            }
            if (lines.size() > 3) alfa = Double.parseDouble(lines.get(3).split(" ")[0]);
            if (lines.size() > 4) beta = Double.parseDouble(lines.get(4).split(" ")[0]);
            if (lines.size() > 5) gama = Double.parseDouble(lines.get(5).split(" ")[0]);
            if (lines.size() > 6) sigma = Double.parseDouble(lines.get(6).split(" ")[0]);
            if (lines.size() > 7) step = Double.parseDouble(lines.get(7).split(" ")[0]);
            if (lines.size() > 8) dX = Double.parseDouble(lines.get(8).split(" ")[0]);
        } catch (IOException e) {
        }

        if (x01 == null) x01 = Arrays.asList(5.0, 5.0);
        if (x02 == null) x02 = Arrays.asList(20.0, 20.0);
        if (epsilon == null) epsilon = DEFAULT_EPSILON;
        if (alfa == null) alfa = DEFAULT_ALPHA;
        if (beta == null) beta = DEFAULT_BETA;
        if (gama == null) gama = DEFAULT_GAMA;
        if (sigma == null) sigma = DEFAULT_SIGMA;
        if (step == null) step = 1.0;
        if (dX == null) dX = DEFAULT_DX;

        System.out.println("x0: " + x01);

        Function f = Function.function1();

        for (int i = 1; i < 20; i++) {
            f.reset();
            List<Double> res = simplex(x01, epsilon, f, alfa, beta, gama, sigma, i, false);

            System.out.printf("Simplex result (step =" + i + "):" + formatList(res) + ", fmin: " + f.doFunction(res) + ", counter: " + f.getCounter() + "\n");
        }

        System.out.println();
        System.out.println("x0: " + x02);

        for (int i = 1; i < 20; i++) {
            f.reset();
            List<Double> res = simplex(x02, epsilon, f, alfa, beta, gama, sigma, i, false);

            System.out.printf("Simplex result (step =" + i + "):" + formatList(res) + ", fmin: " + f.doFunction(res) + ", counter: " + f.getCounter() + "\n");
        }

        System.out.println("==============================================================================");
        System.out.println();
    }

    private static void zadatak5() {
        System.out.println("==============================================================================");
        System.out.println("Zadatak 5:");
        System.out.println("==============================================================================");

        Random r = new Random();

        int cnt = 0;
        int brIter = 1000;

        Function f = Function.function6();

        for (int i = 0; i < brIter; i++) {
            double x1 = -50 + 100 * r.nextDouble();
            double x2 = -50 + 100 * r.nextDouble();

            List<Double> x0 = Arrays.asList(x1, x2);

            System.out.print("x0: " + formatList(x0));

            f.reset();
            List<Double> res = simplex(x0, 1e-6, f, 1, 0.5, 2, 0.5, i, false);

            if (checkRes(res)) {
                cnt++;
            }

            System.out.print(" Simplex result:" + formatList(res) + " fmin: " + f.doFunction(res) + "\n");
        }

        System.out.println("Vjerojatnost pronalaska minimuma (" + cnt + " / " + brIter + ") = " + (double) cnt / brIter);

        System.out.println("==============================================================================");
        System.out.println();
    }
}
