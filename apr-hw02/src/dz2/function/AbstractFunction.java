package dz2.function;

public abstract class AbstractFunction<T, V> {
    int counter;

    abstract V doFunction(T x);

    public int getCounter() {
        return counter;
    }

    public void reset() {
        counter = 0;
    }
}